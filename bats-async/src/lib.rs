use std::{collections::HashMap, path::Path, time::Duration};

use anyhow::Result;
use bats_dsp::sample_rate::SampleRate;
use bats_lib::{
    any_plugin::AnyPlugin,
    builder::{BatsBuilder, PluginBuilder, TrackBuilder},
    plugin::{metadata::PluginParamMetadata, MidiEvent},
    track::Track,
    Bats,
};
use channels::CommandSender;
use command::Command;
use log::{error, info};
use notification::Notification;
use parking_lot::Mutex;

pub mod channels;
pub mod command;
pub mod notification;

/// The minimum BPM.
const MIN_BPM: f32 = 10.0;
/// The maximum BPM.
const MAX_BPM: f32 = 360.0;

/// Contains state for dealing with
#[derive(Debug)]
pub struct BatsState {
    /// The sample rate.
    sample_rate: SampleRate,
    /// The buffer size.
    buffer_size: usize,
    /// Used to send commands to bats.
    commands: CommandSender,
    /// The inner state.
    state: Mutex<InnerState>,
}

#[derive(Clone, Debug, PartialEq)]
struct InnerState {
    /// The armed track.
    armed_track: usize,
    /// True if playback is enabled.
    playback_enabled: bool,
    /// True if recording is enabled.
    recording_enabled: bool,
    /// The current BPM.
    bpm: f32,
    /// The volume of the metronome.
    metronome_volume: f32,
    /// Details for all the tracks.
    tracks: [TrackDetails; Bats::SUPPORTED_TRACKS],
}

/// Contains track details.
#[derive(Clone, Debug, PartialEq)]
pub struct TrackDetails {
    /// The id of the track.
    pub id: usize,
    /// The name of the plugin.
    pub plugin_name: &'static str,
    /// The parameters for the plugin.
    pub plugin_params: Vec<PluginParamMetadata>,
    /// The volume for the track.
    pub volume: f32,
    /// The track parameters.
    pub params: HashMap<u32, f32>,
    /// The sequence on the track.
    pub sequence: Vec<MidiEvent>,
}

impl TrackDetails {
    /// The minimum track volume.
    const MIN_TRACK_VOLUME: f32 = 0.00786;
    /// The maximum track volume.
    const MAX_TRACK_VOLUME: f32 = 4.0;

    /// Create a new `PluginDetails` from a `PluginInstance`.
    fn new(id: usize, t: &Track) -> TrackDetails {
        TrackDetails {
            id,
            plugin_name: t.plugin.name(),
            plugin_params: t.plugin.collect_param_metadata(),
            volume: t.volume,
            params: t.plugin.param_id_to_value(),
            sequence: t.sequence.clone(),
        }
    }

    /// Update the metadata to reflect the state of `plugin`.
    pub fn update_plugin(&mut self, plugin: &AnyPlugin) {
        self.plugin_name = plugin.name();
        self.plugin_params = plugin.collect_param_metadata();
        self.params = plugin.param_id_to_value();
    }

    /// Return the human readable title of the track.
    pub fn title(&self) -> String {
        format!(
            "{track_number} - {plugin_name}",
            track_number = self.id + 1,
            plugin_name = self.plugin_name
        )
    }

    /// Get the parameter metadata for the given `param_id` or `None` if it could not be found.
    pub fn param_by_id(&self, param_id: u32) -> Option<PluginParamMetadata> {
        self.plugin_params
            .iter()
            .find(|p| p.id == param_id)
            .copied()
    }
}

impl BatsState {
    /// Create a new `BatsState`.
    pub fn new(bats: &Bats, commands: CommandSender) -> BatsState {
        BatsState {
            commands,
            sample_rate: bats.sample_rate,
            buffer_size: bats.buffer_size,
            state: InnerState::new(bats).into(),
        }
    }

    /// Save the current state to the given path.
    pub fn save(&self, path: impl AsRef<Path>) -> Result<()> {
        let data = self.save_to_bytes();
        if let Some(p) = path.as_ref().parent() {
            let _ = std::fs::create_dir(p);
        }
        std::fs::write(path, data)?;
        Ok(())
    }

    /// Load the current state from the given path.
    pub fn load(&self, path: impl AsRef<Path>) -> Result<()> {
        let bytes = std::fs::read(path)?;
        self.load_from_bytes(&bytes)
    }

    /// Save the current state as bytes.
    fn save_to_bytes(&self) -> Vec<u8> {
        self.update_sequences();
        let state = self.state.lock();
        let builder = BatsBuilder {
            sample_rate: self.sample_rate,
            buffer_size: self.buffer_size,
            bpm: state.bpm,
            tracks: core::array::from_fn(|idx| {
                let t = &state.tracks[idx];
                let params = t.params.clone();
                let sequence = t.sequence.clone();
                let plugin = PluginBuilder::from_name_and_params(t.plugin_name, params).unwrap();
                TrackBuilder {
                    plugin,
                    volume: t.volume,
                    sequence,
                }
            }),
        };
        serde_json::to_vec(&builder).expect("Failed to convert save data to json.")
    }

    /// Load the state from the bytes.
    fn load_from_bytes(&self, bytes: &[u8]) -> Result<()> {
        let mut builder: BatsBuilder = serde_json::from_slice(bytes)?;
        builder.sample_rate = self.sample_rate;
        builder.buffer_size = self.buffer_size;
        let bats = Box::new(builder.build());
        *self.state.lock() = InnerState::new(&bats);
        self.commands.send(Command::SetBats(bats));
        Ok(())
    }

    /// Handle all notifications.
    fn handle_notifications(&self) {
        self.handle_notifications_impl(false);
    }

    /// Wait for all commands in the command queue to be handled by the bats processor.
    pub fn wait_for_command_queue(&self) {
        self.commands.send(Command::SendAck);
        self.handle_notifications_impl(true);
    }

    fn handle_notifications_impl(&self, wait_for_ack: bool) {
        let mut has_ack = !wait_for_ack;
        loop {
            for notification in self.commands.notifications() {
                match notification {
                    Notification::Undo(cmd) => {
                        // TODO: Implement undo functionality.
                        drop(cmd);
                    }
                    Notification::SequenceSync(sequences) => {
                        self.state.lock().update_sequences(sequences.into_iter());
                    }
                    Notification::Ack => has_ack = true,
                }
            }
            if has_ack {
                return;
            }
            std::thread::sleep(Duration::from_millis(10));
        }
    }

    /// Get the sample rate.
    pub fn sample_rate(&self) -> SampleRate {
        self.handle_notifications();
        self.sample_rate
    }

    /// Get the buffer size.
    pub fn buffer_size(&self) -> usize {
        self.handle_notifications();
        self.buffer_size
    }

    /// Set the plugin for the track.
    pub fn set_plugin(&self, track_id: usize, plugin: AnyPlugin) {
        self.handle_notifications();
        info!(
            "Setting track {track_id} plugin to {plugin_name}.",
            plugin_name = plugin.name()
        );
        match self.state.lock().tracks.get_mut(track_id) {
            None => {
                error!("Could not find track with id {track_id}.");
            }
            Some(track) => {
                track.update_plugin(&plugin);
                self.commands.send(Command::SetPlugin { track_id, plugin });
            }
        }
    }

    /// Return the currently armed track.
    pub fn armed(&self) -> usize {
        self.handle_notifications();
        self.state.lock().armed_track
    }

    /// Returns the currently armed track.
    pub fn armed_track(&self) -> TrackDetails {
        self.handle_notifications();
        let state = self.state.lock();
        state.tracks[state.armed_track].clone()
    }

    /// Set the armed plugin by id.
    fn set_armed(&self, armed: usize) {
        self.handle_notifications();
        let mut state = self.state.lock();
        if state.armed_track == armed {
            return;
        }
        state.armed_track = armed;
        self.commands.send(Command::SetArmedTrack(armed));
    }

    /// Scroll the armed tracks by amount.
    pub fn scroll_armed(&self, amount: isize) {
        if amount < 0 {
            self.scroll_armed(amount + Bats::SUPPORTED_TRACKS as isize);
            return;
        }
        self.set_armed((self.armed() + amount as usize) % Bats::SUPPORTED_TRACKS);
    }

    /// True if playback is enabled.
    pub fn playback_enabled(&self) -> bool {
        self.handle_notifications();
        self.state.lock().playback_enabled
    }

    /// Set the playback.
    pub fn set_playback(&self, enabled: bool) {
        self.handle_notifications();
        let mut state = self.state.lock();
        if state.playback_enabled == enabled {
            return;
        }
        state.playback_enabled = enabled;
        self.commands.send(Command::SetPlayback(enabled));
    }

    /// Set the playback.
    pub fn toggle_playback(&self) {
        let enabled = !self.state.lock().playback_enabled;
        self.set_playback(enabled);
    }

    /// True if recording is enabled.
    pub fn recording_enabled(&self) -> bool {
        self.handle_notifications();
        self.state.lock().recording_enabled
    }

    /// Toggle if recording is enabled.
    pub fn toggle_recording(&self) {
        let enabled = !self.state.lock().recording_enabled;
        self.set_recording(enabled);
    }

    /// Set if recording is enabled.
    pub fn set_recording(&self, enabled: bool) {
        self.handle_notifications();
        let mut state = self.state.lock();
        if state.recording_enabled == enabled {
            return;
        }
        state.recording_enabled = enabled;
        self.commands.send(Command::SetRecord(enabled));
    }

    /// Set the track volume.
    pub fn modify_track_volume(&self, track_id: usize, f: impl Fn(&TrackDetails) -> f32) {
        self.handle_notifications();
        if let Some(t) = self.state.lock().tracks.get_mut(track_id) {
            t.volume = f(t).clamp(
                TrackDetails::MIN_TRACK_VOLUME,
                TrackDetails::MAX_TRACK_VOLUME,
            );
            self.commands.send(Command::SetTrackVolume {
                track_id,
                volume: t.volume,
            });
        }
    }

    /// Modify the bpm.
    pub fn modify_bpm(&self, f: impl Fn(f32) -> f32) {
        self.handle_notifications();
        let mut state = self.state.lock();
        state.bpm = f(state.bpm).clamp(MIN_BPM, MAX_BPM);
        self.commands.send(Command::SetTransportBpm(state.bpm));
    }

    /// The current BPM.
    pub fn bpm(&self) -> f32 {
        self.handle_notifications();
        self.state.lock().bpm
    }

    /// Modify the metronome volume.
    pub fn modify_metronome_volume(&self, f: impl Fn(f32) -> f32) {
        self.handle_notifications();
        let mut state = self.state.lock();
        let v = f(state.metronome_volume).clamp(0.0, 1.0);
        state.metronome_volume = v;
        self.commands
            .send(Command::SetMetronomeVolume(state.metronome_volume));
    }

    /// Get the metronome volume.
    pub fn metronome_volume(&self) -> f32 {
        self.handle_notifications();
        self.state.lock().metronome_volume
    }

    /// Get all the tracks.
    pub fn tracks_vec(&self) -> Vec<TrackDetails> {
        self.handle_notifications();
        self.state.lock().tracks.to_vec()
    }

    /// Get a track by its id.
    pub fn track_by_id(&self, track_id: usize) -> Option<TrackDetails> {
        self.handle_notifications();
        self.state.lock().tracks.get(track_id).cloned()
    }

    /// Get the param value for the given `param_id` for `track_id`.
    pub fn param(&self, track_id: usize, param_id: u32) -> f32 {
        self.handle_notifications();
        let state = self.state.lock();
        let track = match state.tracks.get(track_id) {
            Some(t) => t,
            None => {
                error!("Attempted to get track for invalid track id {track_id}.");
                return 0.0;
            }
        };
        let get_default_value = || match track.param_by_id(param_id) {
            None => 0.0,
            Some(p) => p.default_value,
        };
        track
            .params
            .get(&param_id)
            .copied()
            .unwrap_or_else(get_default_value)
    }

    /// Modify the param value by applying `f`. This function also handles keeping the param valid,
    /// like adjusting according to the min and max values.
    pub fn modify_param(&self, track_id: usize, param_id: u32, f: impl Fn(f32) -> f32) {
        self.handle_notifications();
        let mut state = self.state.lock();
        let track = match state.tracks.get_mut(track_id) {
            None => {
                error!("Could not find track {track_id} to modify param {param_id}.");
                return;
            }
            Some(t) => t,
        };
        let param = match track.param_by_id(param_id) {
            Some(p) => p,
            None => {
                error!(
                    "Could not find param with id {param_id} for track with plugin {plugin_name}.",
                    plugin_name = track.plugin_name
                );
                return;
            }
        };
        let current_value = *track.params.get(&param_id).unwrap();
        let value = f(current_value).clamp(param.min_value, param.max_value);
        track.params.insert(param_id, value);
        self.commands.send(Command::SetParam {
            track_id,
            param_id,
            value,
        });
    }

    /// Set the sequence for the track.
    pub fn set_sequence(&self, track_id: usize, mut sequence: Vec<MidiEvent>) {
        self.handle_notifications();
        let mut state = self.state.lock();
        let track_state = match state.tracks.get_mut(track_id) {
            Some(t) => t,
            None => {
                error!("set_sequence on track_id {track_id} is not valid.");
                return;
            }
        };
        track_state.sequence = sequence.clone();
        // TODO: Reserve the right amount. This will reserve additional capacity instead of setting
        // the total capacity.
        sequence.reserve(Track::SEQUENCE_CAPACITY);
        self.commands
            .send(Command::SetSequence { track_id, sequence });
    }

    /// Update the sequences stored in the track.
    pub fn update_sequences(&self) {
        let sequence_map = self
            .state
            .lock()
            .tracks
            .iter()
            .map(|t| (t.id, Vec::with_capacity(Track::SEQUENCE_CAPACITY)))
            .collect();
        self.commands.send(Command::SyncSequences(sequence_map));
        self.wait_for_command_queue();
    }
}

impl InnerState {
    /// Create a new `InnerState`.
    pub fn new(bats: &Bats) -> InnerState {
        let bpm = bats.transport.bpm();
        let tracks = core::array::from_fn(|idx| TrackDetails::new(idx, &bats.tracks[idx]));
        InnerState {
            armed_track: bats.armed_track,
            playback_enabled: bats.playback_enabled,
            recording_enabled: bats.recording_enabled,
            bpm,
            metronome_volume: bats.transport.metronome_volume,
            tracks,
        }
    }

    pub fn update_sequences(&mut self, sequences: impl Iterator<Item = (usize, Vec<MidiEvent>)>) {
        for (track_id, sequence) in sequences {
            self.tracks[track_id].sequence = sequence;
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use bats_dsp::position::Position;
    use bats_lib::plugin::{
        empty::Empty,
        toof::{Toof, ToofParam},
        BatsInstrument, BatsPluginParam,
    };
    use bmidi::{Channel, MidiMessage, Note, U7};
    use pretty_assertions::{assert_eq, assert_ne};
    use tempdir::TempDir;

    use crate::channels::new_async_commander;

    use super::*;

    #[test]
    fn track_titles_are_unique() {
        let mut known_titles = HashSet::new();
        let state = BatsState::new(&BatsBuilder::default().build(), new_async_commander().0);
        for track in state.state.into_inner().tracks.into_iter() {
            let title = track.title();
            let is_duplicate = known_titles.contains(&title);
            assert!(!is_duplicate);
            known_titles.insert(title);
        }
    }

    #[test]
    fn save_and_load_restores_state() {
        let test_path = TempDir::new("save-and-load-restores-state")
            .unwrap()
            .path()
            .join("state.json");

        let (sender, receiver) = new_async_commander();
        let mut initial_bats = BatsBuilder::default().build();
        initial_bats.tracks[3].sequence = vec![
            MidiEvent {
                position: Position::new(0.4),
                midi: MidiMessage::NoteOn(Channel::Ch1, Note::C3, U7::MAX),
            },
            MidiEvent {
                position: Position::new(2.4),
                midi: MidiMessage::NoteOff(Channel::Ch1, Note::C3, U7::MIN),
            },
        ];
        initial_bats.tracks[4].volume = 0.4;
        initial_bats.tracks[5].plugin = Toof::new(initial_bats.sample_rate).into();
        initial_bats.tracks[5]
            .plugin
            .set_param_by_id(ToofParam::FilterCutoff.metadata().id, 256.0);
        // The following changes are not saved.
        // initial_bats.armed_track = 4;
        let mut initial_bats_state = BatsState::new(&initial_bats, sender);
        let cancel_execute = receiver.execute_async(initial_bats);
        initial_bats_state.save(&test_path).unwrap();
        let (_, initial_bats) = cancel_execute();

        let (sender, receiver) = new_async_commander();
        let mut new_bats = BatsBuilder::default().build();
        let mut new_bats_state = BatsState::new(&new_bats, sender);
        assert_ne!(&initial_bats, &new_bats);
        assert_ne!(
            initial_bats_state.state.get_mut(),
            new_bats_state.state.get_mut()
        );

        new_bats_state.load(&test_path).unwrap();
        receiver.execute_all(&mut new_bats);
        assert_eq!(&initial_bats, &new_bats);
        assert_eq!(
            initial_bats_state.state.get_mut(),
            new_bats_state.state.get_mut()
        );
    }

    #[test]
    fn load_on_bad_path_returns_error() {
        let test_path = TempDir::new("load-on-bad-path-returns-error")
            .unwrap()
            .path()
            .join("does-not-exist.json");

        let (sender, _) = new_async_commander();
        let bats = BatsBuilder::default().build();
        let state = BatsState::new(&bats, sender);
        assert!(state.load(&test_path).is_err());
    }

    #[test]
    fn can_get_sample_rate() {
        let (sender, _) = new_async_commander();
        let bats = BatsBuilder {
            sample_rate: SampleRate::new(1234.0),
            ..BatsBuilder::default()
        }
        .build();
        let state = BatsState::new(&bats, sender);
        assert_eq!(state.sample_rate(), SampleRate::new(1234.0));
    }

    #[test]
    fn can_get_buffer_size() {
        let (sender, _) = new_async_commander();
        let bats = BatsBuilder {
            buffer_size: 5678,
            ..BatsBuilder::default()
        }
        .build();
        let state = BatsState::new(&bats, sender);
        assert_eq!(state.buffer_size(), 5678);
    }

    #[test]
    fn modify_bpm_modifies_bpm() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder {
            bpm: 90.0,
            ..BatsBuilder::default()
        }
        .build();
        let state = BatsState::new(&bats, sender);

        assert_eq!(state.bpm(), 90.0);
        assert_eq!(bats.transport.bpm(), 90.0);

        state.modify_bpm(|bpm| bpm * 2.0);
        receiver.execute_all(&mut bats);

        assert_eq!(state.bpm(), 180.0);
        assert_eq!(bats.transport.bpm(), 180.0);
    }

    #[test]
    fn modify_bpm_out_of_range_is_clamped() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder {
            ..BatsBuilder::default()
        }
        .build();
        let state = BatsState::new(&bats, sender);

        state.modify_bpm(|_| 0.0);
        receiver.execute_all(&mut bats);
        assert_eq!(state.bpm(), MIN_BPM);
        assert_eq!(bats.transport.bpm(), MIN_BPM);

        state.modify_bpm(|_| 1000000.0);
        receiver.execute_all(&mut bats);
        assert_eq!(state.bpm(), MAX_BPM);
        assert_eq!(bats.transport.bpm(), MAX_BPM);
    }

    #[test]
    fn set_plugin_on_bad_track_id_does_nothing() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let initial_bats = bats.clone();
        let state = BatsState::new(&bats, sender);
        let initial_metadata = state.tracks_vec();
        assert_eq!(bats, initial_bats);
        assert_eq!(state.tracks_vec(), initial_metadata);

        let bad_track_id = 1000; // Out of range.
        state.set_plugin(bad_track_id, AnyPlugin::Toof(Toof::new(bats.sample_rate)));
        receiver.execute_all(&mut bats);
        assert_eq!(bats, initial_bats);
        assert_eq!(state.tracks_vec(), initial_metadata);
    }

    #[test]
    fn set_plugin_sets_the_plugin() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let state = BatsState::new(&bats, sender);
        let track_id = 3;
        assert_eq!(bats.tracks[track_id].plugin.name(), Empty::NAME);
        assert_eq!(
            state.track_by_id(track_id).unwrap(),
            TrackDetails {
                id: track_id,
                plugin_name: "empty",
                plugin_params: vec![],
                volume: 1.0,
                params: HashMap::new(),
                sequence: vec![],
            }
        );

        state.set_plugin(track_id, AnyPlugin::Toof(Toof::new(bats.sample_rate)));
        receiver.execute_all(&mut bats);
        assert_eq!(bats.tracks[track_id].plugin.name(), Toof::NAME);
        assert_eq!(
            state.track_by_id(track_id).unwrap(),
            TrackDetails {
                id: track_id,
                plugin_name: "toof",
                plugin_params: ToofParam::all().map(|p| p.metadata()).collect(),
                volume: 1.0,
                params: bats.tracks[track_id].plugin.param_id_to_value(),
                sequence: vec![],
            }
        );
    }

    #[test]
    fn scroll_armed_sets_armed_track() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let state = BatsState::new(&bats, sender);
        assert_eq!(bats.armed_track, 0);
        assert_eq!(state.armed(), 0);

        state.scroll_armed(4);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.armed_track, 4);
        assert_eq!(state.armed(), 4);
        assert_eq!(
            state.armed_track(),
            TrackDetails {
                id: 4,
                plugin_name: "empty",
                plugin_params: vec![],
                volume: 1.0,
                params: HashMap::new(),
                sequence: vec![],
            }
        );
    }

    #[test]
    fn scroll_armed_on_large_values_wraps_around() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let state = BatsState::new(&bats, sender);
        assert_eq!(bats.armed_track, 0);
        assert_eq!(state.armed(), 0);

        state.scroll_armed(8 + 8 + 3);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.armed_track, 3);
        assert_eq!(state.armed(), 3);
    }

    #[test]
    fn scroll_armed_on_negative_values_decrements() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        bats.armed_track = 6;
        let state = BatsState::new(&bats, sender);
        assert_eq!(bats.armed_track, 6);
        assert_eq!(state.armed(), 6);

        state.scroll_armed(-3);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.armed_track, 3);
        assert_eq!(state.armed(), 3);
    }

    #[test]
    fn set_playback_sets_the_playback() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        bats.playback_enabled = true;
        assert_eq!(
            BatsState::new(&bats, new_async_commander().0).playback_enabled(),
            true
        );
        bats.playback_enabled = false;
        assert_eq!(
            BatsState::new(&bats, new_async_commander().0).playback_enabled(),
            false
        );
        let bats_state = BatsState::new(&bats, sender);

        bats_state.set_playback(true);
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.playback_enabled(), true);
        assert_eq!(bats.playback_enabled, true);

        bats_state.set_playback(true);
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.playback_enabled(), true);
        assert_eq!(bats.playback_enabled, true);

        bats_state.set_playback(false);
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.playback_enabled(), false);
        assert_eq!(bats.playback_enabled, false);

        bats_state.toggle_playback();
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.playback_enabled(), true);
        assert_eq!(bats.playback_enabled, true);

        bats_state.toggle_playback();
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.playback_enabled(), false);
        assert_eq!(bats.playback_enabled, false);
    }

    #[test]
    fn set_recording_sets_the_recording() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        bats.recording_enabled = true;
        assert_eq!(
            BatsState::new(&bats, new_async_commander().0).recording_enabled(),
            true
        );
        bats.recording_enabled = false;
        assert_eq!(
            BatsState::new(&bats, new_async_commander().0).recording_enabled(),
            false
        );
        let bats_state = BatsState::new(&bats, sender);

        bats_state.set_recording(true);
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.recording_enabled(), true);
        assert_eq!(bats.recording_enabled, true);

        bats_state.set_recording(true);
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.recording_enabled(), true);
        assert_eq!(bats.recording_enabled, true);

        bats_state.set_recording(false);
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.recording_enabled(), false);
        assert_eq!(bats.recording_enabled, false);

        bats_state.toggle_recording();
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.recording_enabled(), true);
        assert_eq!(bats.recording_enabled, true);

        bats_state.toggle_recording();
        receiver.execute_all(&mut bats);
        assert_eq!(bats_state.recording_enabled(), false);
        assert_eq!(bats.recording_enabled, false);
    }

    #[test]
    fn scroll_armed_on_negative_values_wraps_around() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let state = BatsState::new(&bats, sender);
        assert_eq!(bats.armed_track, 0);
        assert_eq!(state.armed(), 0);

        state.scroll_armed(-8 - 8 - 3);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.armed_track, 5);
        assert_eq!(state.armed(), 5);
    }

    #[test]
    fn modify_track_volume_modifies_track_volume() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        bats.tracks[2].volume = 0.25;
        let state = BatsState::new(&bats, sender);
        assert_eq!(state.track_by_id(2).unwrap().volume, 0.25);

        state.modify_track_volume(2, |t| t.volume + 1.0);
        receiver.execute_all(&mut bats);
        assert_eq!(state.track_by_id(2).unwrap().volume, 1.25);
        assert_eq!(bats.tracks[2].volume, 1.25);
    }

    #[test]
    fn modify_track_volume_out_of_range_is_clamped() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let state = BatsState::new(&bats, sender);

        state.modify_track_volume(2, |_| -1.0);
        receiver.execute_all(&mut bats);
        assert_eq!(
            state.track_by_id(2).unwrap().volume,
            TrackDetails::MIN_TRACK_VOLUME
        );
        assert_eq!(bats.tracks[2].volume, TrackDetails::MIN_TRACK_VOLUME);

        state.modify_track_volume(2, |_| 1000.0);
        receiver.execute_all(&mut bats);
        assert_eq!(
            state.track_by_id(2).unwrap().volume,
            TrackDetails::MAX_TRACK_VOLUME
        );
        assert_eq!(bats.tracks[2].volume, TrackDetails::MAX_TRACK_VOLUME);
    }

    #[test]
    fn modify_track_volume_on_bad_track_id_does_nothing() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let initial_bats = bats.clone();
        let state = BatsState::new(&bats, sender);
        let initial_tracks = state.tracks_vec();

        state.modify_track_volume(100, |t| t.volume + 2.0);
        receiver.execute_all(&mut bats);
        assert_eq!(state.tracks_vec(), initial_tracks);
        assert_eq!(bats, initial_bats);
    }

    #[test]
    fn modify_metronome_volume_modifies_metronome_volume() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        bats.transport.metronome_volume = 0.5;
        let state = BatsState::new(&bats, sender);
        assert_eq!(state.metronome_volume(), 0.5);

        state.modify_metronome_volume(|v| v + 0.25);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.transport.metronome_volume, 0.75);
        assert_eq!(state.metronome_volume(), 0.75);
    }

    #[test]
    fn modify_metronome_volume_clamps_to_0_and_1() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        bats.transport.metronome_volume = 0.5;
        let state = BatsState::new(&bats, sender);
        assert_eq!(state.metronome_volume(), 0.5);

        state.modify_metronome_volume(|_| -100.0);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.transport.metronome_volume, 0.0);
        assert_eq!(state.metronome_volume(), 0.0);

        state.modify_metronome_volume(|_| 100.0);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.transport.metronome_volume, 1.0);
        assert_eq!(state.metronome_volume(), 1.0);
    }

    #[test]
    fn modify_param_modifies_param() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let param = ToofParam::VelocitySensitivity.metadata();
        bats.tracks[1].plugin = Toof::new(bats.sample_rate).into();
        bats.tracks[1].plugin.set_param_by_id(param.id, 0.1);
        let state = BatsState::new(&bats, sender);
        assert_eq!(state.param(1, param.id), 0.1);

        state.modify_param(1, param.id, |v| v + 0.1);
        receiver.execute_all(&mut bats);
        assert_eq!(state.param(1, param.id), 0.2);
        assert_eq!(bats.tracks[1].plugin.param_by_id(param.id).unwrap(), 0.2);
    }

    #[test]
    fn modify_param_with_out_of_range_values_clamps_value() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let param = ToofParam::VelocitySensitivity.metadata();
        bats.tracks[1].plugin = Toof::new(bats.sample_rate).into();
        bats.tracks[1].plugin.set_param_by_id(param.id, 0.1);
        let state = BatsState::new(&bats, sender);

        state.modify_param(1, param.id, |_| f32::MIN);
        receiver.execute_all(&mut bats);
        assert_eq!(state.param(1, param.id), param.min_value);
        assert_eq!(
            bats.tracks[1].plugin.param_by_id(param.id).unwrap(),
            param.min_value
        );

        state.modify_param(1, param.id, |_| f32::MAX);
        receiver.execute_all(&mut bats);
        assert_eq!(state.param(1, param.id), param.max_value);
        assert_eq!(
            bats.tracks[1].plugin.param_by_id(param.id).unwrap(),
            param.max_value
        );
    }

    #[test]
    fn modify_param_on_bad_args_does_nothing() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let initial_bats = bats.clone();
        let state = BatsState::new(&bats, sender);
        let initial_tracks = state.tracks_vec();

        // Bad track.
        state.modify_param(1000, 0, |v| v + 0.1);
        receiver.execute_all(&mut bats);
        assert_eq!(state.tracks_vec(), initial_tracks);
        assert_eq!(bats, initial_bats);

        // Bad param.
        state.modify_param(0, 100, |v| v + 0.1);
        receiver.execute_all(&mut bats);
        assert_eq!(state.tracks_vec(), initial_tracks);
        assert_eq!(bats, initial_bats);
    }

    #[test]
    fn param_by_id_returns_plugin_metadata() {
        let mut bats = BatsBuilder::default().build();
        bats.tracks[0].plugin = Toof::new(bats.sample_rate).into();
        let (sender, _) = new_async_commander();
        let state = BatsState::new(&bats, sender);
        for param in ToofParam::all() {
            let param_id = param.metadata().id;
            assert_eq!(
                state.state.lock().tracks[0].param_by_id(param_id),
                Some(param.metadata())
            );
        }
    }

    #[test]
    fn set_sequence_sets_the_sequence() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let state = BatsState::new(&bats, sender);
        assert_eq!(bats.tracks[0].sequence, vec![]);
        assert_eq!(state.track_by_id(0).unwrap().sequence, vec![]);

        let midi_event = MidiEvent {
            position: Position::new(1.0),
            midi: MidiMessage::Start,
        };
        state.set_sequence(0, vec![midi_event]);
        receiver.execute_all(&mut bats);
        assert_eq!(bats.tracks[0].sequence, vec![midi_event]);
        assert_eq!(state.track_by_id(0).unwrap().sequence, vec![midi_event]);
    }

    #[test]
    fn set_sequence_on_bad_track_id_does_nothing() {
        let (sender, receiver) = new_async_commander();
        let mut bats = BatsBuilder::default().build();
        let initial_bats = bats.clone();
        let state = BatsState::new(&bats, sender);
        let initial_tracks = state.tracks_vec();

        state.modify_param(1000, 0, |v| v + 0.1);
        state.set_sequence(
            1000,
            vec![MidiEvent {
                position: Position::new(1.0),
                midi: MidiMessage::Start,
            }],
        );
        receiver.execute_all(&mut bats);
        assert_eq!(state.tracks_vec(), initial_tracks);
        assert_eq!(bats, initial_bats);
    }

    #[test]
    fn bats_state_has_debug() {
        let state = BatsState::new(&BatsBuilder::default().build(), new_async_commander().0);
        assert_ne!(format!("{state:?}"), "");
    }
}
