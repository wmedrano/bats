use std::collections::HashMap;

use bats_lib::plugin::MidiEvent;

use crate::command::Command;

#[derive(Clone, Debug, PartialEq)]
/// A notification for the UI.
pub enum Notification {
    /// Notify that a new undo command is available.
    Undo(Command),
    /// A mapping from track to the respective sequence.
    SequenceSync(HashMap<usize, Vec<MidiEvent>>),
    /// Acknowledge that the command was processed.
    Ack,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn notification_has_debug() {
        let n = Notification::Ack;
        let debug_text = format!("{:?}", n.clone());
        assert!(!debug_text.is_empty());
    }

    #[test]
    fn notification_size_is_reasonable() {
        let size = std::mem::size_of::<Notification>();
        assert_eq!(size, 56);
    }
}
