use std::collections::HashMap;

use bats_lib::{any_plugin::AnyPlugin, plugin::MidiEvent, Bats};
use log::error;

use crate::notification::Notification;

/// Contains commands for bats.
#[derive(Clone, Debug, PartialEq)]
pub enum Command {
    /// No command.
    None,
    /// Set the metrenome.
    SetMetronomeVolume(f32),
    /// Set the BPM of the transport.
    SetTransportBpm(f32),
    /// Add a new track.
    SetPlugin { track_id: usize, plugin: AnyPlugin },
    /// Set the armed track.
    SetArmedTrack(usize),
    /// Set the track volume.
    SetTrackVolume { track_id: usize, volume: f32 },
    /// Set a parameter.
    SetParam {
        track_id: usize,
        param_id: u32,
        value: f32,
    },
    /// Set the sequence for the track.
    SetSequence {
        track_id: usize,
        sequence: Vec<MidiEvent>,
    },
    /// Set if recording is enabled or disabled.
    SetRecord(bool),
    /// Set if playback is enabled.
    SetPlayback(bool),
    /// Set the state of bats to the given state.
    SetBats(Box<Bats>),
    /// Sync the sequences for the tracks in the hash map and send the results through a notification.
    SyncSequences(HashMap<usize, Vec<MidiEvent>>),
    /// Send an acknowledgement notification as soon as this command is processed.
    SendAck,
}

impl Command {
    /// The command to execute. It returns the command to undo the current command.
    pub fn execute(self, b: &mut Bats, send_notification: impl Fn(Notification)) -> Command {
        match self {
            Command::None => Command::None,
            Command::SetMetronomeVolume(v) => {
                let old = b.transport.metronome_volume;
                b.transport.metronome_volume = v;
                Command::SetMetronomeVolume(old)
            }
            Command::SetTransportBpm(bpm) => {
                let previous_bpm = b.transport.bpm();
                b.transport.set_bpm(b.sample_rate, bpm);
                Command::SetTransportBpm(previous_bpm)
            }
            Command::SetPlugin { track_id, plugin } => match b.tracks.get_mut(track_id) {
                None => Command::None,
                Some(t) => {
                    let mut old_plugin = plugin;
                    std::mem::swap(&mut t.plugin, &mut old_plugin);
                    Command::SetPlugin {
                        track_id,
                        plugin: old_plugin,
                    }
                }
            },
            Command::SetTrackVolume { track_id, volume } => match b.tracks.get_mut(track_id) {
                None => Command::None,
                Some(t) => {
                    let undo = Command::SetTrackVolume {
                        track_id,
                        volume: t.volume,
                    };
                    t.volume = volume;
                    undo
                }
            },
            Command::SetArmedTrack(armed) => {
                let undo = Command::SetArmedTrack(b.armed_track);
                b.armed_track = armed;
                undo
            }
            Command::SetParam {
                track_id,
                param_id,
                value,
            } => match b.tracks.get_mut(track_id) {
                Some(t) => {
                    let undo = Command::SetParam {
                        track_id,
                        param_id,
                        value: t.plugin.param_by_id(param_id).unwrap_or_default(),
                    };
                    t.plugin.set_param_by_id(param_id, value);
                    undo
                }
                None => {
                    error!(
                        "track {} does not exist, will not set param {} to {}.",
                        track_id, param_id, value
                    );
                    Command::None
                }
            },
            Command::SetSequence {
                track_id,
                mut sequence,
            } => match b.tracks.get_mut(track_id) {
                Some(t) => {
                    std::mem::swap(&mut sequence, &mut t.sequence);
                    Command::SetSequence { track_id, sequence }
                }
                None => {
                    error!("track {track_id} does not exist, will not clear the sequence.");
                    Command::None
                }
            },
            Command::SetRecord(enabled) => {
                let undo = Command::SetRecord(b.recording_enabled);
                b.recording_enabled = enabled;
                undo
            }
            Command::SetPlayback(playback) => {
                let undo = Command::SetPlayback(b.playback_enabled);
                b.playback_enabled = playback;
                if !playback {
                    for track in b.tracks.iter_mut() {
                        track.plugin.reset_midi();
                    }
                }
                undo
            }
            Command::SetBats(mut bats) => {
                std::mem::swap(b, &mut bats);
                Command::SetBats(bats)
            }
            Command::SyncSequences(mut sequences) => {
                for (track_id, sequence) in sequences.iter_mut() {
                    let track = &b.tracks[*track_id];
                    sequence.clear();
                    sequence.extend_from_slice(&track.sequence);
                }
                send_notification(Notification::SequenceSync(sequences));
                Command::None
            }
            Command::SendAck => {
                send_notification(Notification::Ack);
                Command::None
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use bats_dsp::{position::Position, sample_rate::SampleRate};
    use bats_lib::{
        builder::BatsBuilder,
        plugin::{
            empty::Empty,
            toof::{Toof, ToofParam},
            BatsPluginParam,
        },
    };
    use bmidi::MidiMessage;

    use super::*;

    /// Get all the track name for non-empty tracks.
    fn get_track_names(b: &Bats) -> Vec<&'static str> {
        b.tracks.iter().map(|t| t.plugin.name()).collect()
    }

    #[test]
    fn command_size_is_reasonable() {
        let size = std::mem::size_of::<Command>();
        assert_eq!(size, 56);
    }

    #[test]
    fn none_command_undo_is_none() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        let undo = Command::None.execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(undo, Command::None);
    }

    #[test]
    fn set_metronome_volume_sets_new_volume_and_returns_old_as_undo() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.transport.metronome_volume = 1.0;

        let undo = Command::SetMetronomeVolume(0.5)
            .execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.transport.metronome_volume, 0.5);
        assert_eq!(undo, Command::SetMetronomeVolume(1.0));
    }

    #[test]
    fn metrenome_set_bpm() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.transport.set_bpm(b.sample_rate, 100.0);

        let undo =
            Command::SetTransportBpm(90.0).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.transport.bpm(), 90.0);
        assert_eq!(undo, Command::SetTransportBpm(100.0));
    }

    #[test]
    fn set_plugin() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        let plugin = AnyPlugin::Toof(Toof::new(b.sample_rate));
        b.tracks[0].plugin = plugin.clone();
        assert_eq!(
            get_track_names(&b),
            vec!["toof", "empty", "empty", "empty", "empty", "empty", "empty", "empty"]
        );

        let undo = Command::SetPlugin {
            track_id: 1,
            plugin: plugin.clone(),
        }
        .execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(
            get_track_names(&b),
            vec!["toof", "toof", "empty", "empty", "empty", "empty", "empty", "empty"]
        );
        assert_eq!(
            undo,
            Command::SetPlugin {
                track_id: 1,
                plugin: AnyPlugin::Empty(Empty)
            }
        );

        let undo = Command::SetPlugin {
            track_id: 1,
            plugin: AnyPlugin::Empty(Empty),
        }
        .execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(
            get_track_names(&b),
            vec!["toof", "empty", "empty", "empty", "empty", "empty", "empty", "empty"]
        );
        assert_eq!(
            undo,
            Command::SetPlugin {
                track_id: 1,
                plugin: plugin.clone()
            }
        );
    }

    #[test]
    fn remove_plugin_that_does_not_exist_does_nothing() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        let plugin = AnyPlugin::Toof(Toof::new(b.sample_rate));
        b.tracks[0].plugin = plugin.clone();
        b.tracks[2].plugin = plugin.clone();
        assert_eq!(
            get_track_names(&b),
            vec!["toof", "empty", "toof", "empty", "empty", "empty", "empty", "empty"]
        );
        assert_eq!(
            Command::SetPlugin {
                track_id: 1,
                plugin: AnyPlugin::Empty(Empty),
            }
            .execute(&mut b, |_| panic!("notification not expected")),
            Command::SetPlugin {
                track_id: 1,
                plugin: AnyPlugin::default()
            }
        );
        assert_eq!(
            get_track_names(&b),
            vec!["toof", "empty", "toof", "empty", "empty", "empty", "empty", "empty"]
        );
    }

    #[test]
    fn set_armed_track() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.armed_track = 100;

        let undo =
            Command::SetArmedTrack(10).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.armed_track, 10);
        assert_eq!(undo, Command::SetArmedTrack(100));

        let undo =
            Command::SetArmedTrack(20).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.armed_track, 20);
        assert_eq!(undo, Command::SetArmedTrack(10));

        let undo =
            Command::SetArmedTrack(100).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.armed_track, 100);
        assert_eq!(undo, Command::SetArmedTrack(20));
    }

    #[test]
    fn set_track_volume_sets_volume() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.tracks[0].volume = 0.1;
        b.tracks[1].volume = 0.2;
        let undo = Command::SetTrackVolume {
            track_id: 0,
            volume: 0.3,
        }
        .execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(
            undo,
            Command::SetTrackVolume {
                track_id: 0,
                volume: 0.1
            }
        );
        assert_eq!(b.tracks[0].volume, 0.3);
        assert_eq!(b.tracks[1].volume, 0.2);
    }

    #[test]
    fn set_param_sets_param() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.tracks[0].plugin = Toof::new(b.sample_rate).into();
        let param_id = ToofParam::FilterCutoff.metadata().id;
        b.tracks[0].plugin.set_param_by_id(param_id, 500.0);

        assert_eq!(b.tracks[0].plugin.param_by_id(param_id).unwrap(), 500.0);
        let undo = Command::SetParam {
            track_id: 0,
            param_id,
            value: 1000.0,
        }
        .execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(
            undo,
            Command::SetParam {
                track_id: 0,
                param_id,
                value: 500.0,
            }
        );
        assert_eq!(b.tracks[0].plugin.param_by_id(param_id).unwrap(), 1000.0);
    }

    #[test]
    fn set_track_volume_on_track_that_does_not_exist_does_nothing() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        let undo = Command::SetTrackVolume {
            track_id: 1000, // Out of range.
            volume: 0.3,
        }
        .execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(undo, Command::None);
    }

    #[test]
    fn set_sequence_sets_sequence_on_track() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.tracks[4].sequence = vec![MidiEvent {
            position: Position::new(0.0),
            midi: MidiMessage::TuneRequest,
        }];
        let undo = Command::SetSequence {
            track_id: 4,
            sequence: vec![MidiEvent {
                position: Position::new(1.2),
                midi: MidiMessage::Reset,
            }],
        }
        .execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(
            undo,
            Command::SetSequence {
                track_id: 4,
                sequence: vec![MidiEvent {
                    position: Position::new(0.0),
                    midi: MidiMessage::TuneRequest,
                },]
            }
        );
        assert_eq!(
            b.tracks[4].sequence,
            vec![MidiEvent {
                position: Position::new(1.2),
                midi: MidiMessage::Reset
            }]
        );
    }

    #[test]
    fn set_playback() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.playback_enabled = true;

        // true -> true
        let undo =
            Command::SetPlayback(true).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.playback_enabled, true);
        assert_eq!(undo, Command::SetPlayback(true));

        // true -> false
        let undo =
            Command::SetPlayback(false).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.playback_enabled, false);
        assert_eq!(undo, Command::SetPlayback(true));

        // false -> false
        let undo =
            Command::SetPlayback(false).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.recording_enabled, false);
        assert_eq!(undo, Command::SetPlayback(false));

        // false -> true
        let undo =
            Command::SetPlayback(true).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.playback_enabled, true);
        assert_eq!(undo, Command::SetPlayback(false));
    }

    #[test]
    fn set_record() {
        let mut b = BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 64,
            bpm: 120.0,
            tracks: Default::default(),
        }
        .build();
        b.recording_enabled = true;

        // true -> true
        let undo =
            Command::SetRecord(true).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.recording_enabled, true);
        assert_eq!(undo, Command::SetRecord(true));

        // true -> false
        let undo =
            Command::SetRecord(false).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.recording_enabled, false);
        assert_eq!(undo, Command::SetRecord(true));

        // false -> false
        let undo =
            Command::SetRecord(false).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.recording_enabled, false);
        assert_eq!(undo, Command::SetRecord(false));

        // false -> true
        let undo =
            Command::SetRecord(true).execute(&mut b, |_| panic!("notification not expected"));
        assert_eq!(b.recording_enabled, true);
        assert_eq!(undo, Command::SetRecord(false));
    }
}
