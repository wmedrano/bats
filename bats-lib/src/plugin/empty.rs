use bmidi::MidiMessage;

use super::{metadata::NoParams, BatsInstrument};

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
pub struct Empty;

impl BatsInstrument for Empty {
    const NAME: &'static str = "empty";

    type Param = NoParams;

    fn handle_midi(&mut self, _: &MidiMessage) {}

    fn process(&mut self) -> (f32, f32) {
        (0.0, 0.0)
    }
}
