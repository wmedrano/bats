use std::fmt::{Display, Formatter};

use enum_iterator::Sequence;

use super::BatsPluginParam;

/// The type for the parameter.
#[derive(Copy, Clone, Debug, Default, PartialEq)]
pub enum ParamType {
    /// A floating point number.
    #[default]
    Float,
    /// A toggle where >=0.5 is on and <0.5 is off.
    Bool,
    /// A decibel value where 1.0 is 0 dB.
    Decibel,
    /// A percentage between 0% and 100%.
    Percent,
    /// A frequency represented in Hz or kHz.
    Frequency,
    /// A duration.
    Duration,
}

/// Metadata for a plugin parameter.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct PluginParamMetadata {
    /// The id of the plugin parameter.
    pub id: u32,
    /// The name of the plugin parameter.
    pub name: &'static str,
    /// The type for the plugin parameter. Mostly used for display purposes.
    pub param_type: ParamType,
    /// The default value of the plugin parameter.
    pub default_value: f32,
    /// The minimum value of the plugin parameter.
    pub min_value: f32,
    /// The maximum value of the plugin parameter.
    pub max_value: f32,
}

/// A plugin parameter that does nothing.
#[derive(Copy, Clone, Debug, Sequence)]
pub enum NoParams {}

impl BatsPluginParam for NoParams {
    fn metadata(self) -> PluginParamMetadata {
        unreachable!();
    }
}

impl ParamType {
    /// Return the value in a form that can be formatted for display.
    pub fn formatted(&self, value: f32) -> impl Display {
        ParamFormatter {
            param_type: *self,
            value,
        }
    }
}

/// A formatter for params.
struct ParamFormatter {
    /// The param type.
    param_type: ParamType,
    /// The value of the param.
    value: f32,
}

/// Create human readable format for the parameter.
impl Display for ParamFormatter {
    /// Create human readable format for the parameter.
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.param_type {
            ParamType::Float => write!(f, "{}", self.value),
            ParamType::Bool => write!(f, "{}", if self.value < 0.5 { "off" } else { "on" }),
            ParamType::Decibel => {
                let db = 20.0 * self.value.log10();
                if db.abs() < 10.0 {
                    write!(f, "{db:.2} dB")
                } else {
                    write!(f, "{db:.1} dB")
                }
            }
            ParamType::Percent => write!(f, "{:.1}%", self.value * 100.0),
            ParamType::Frequency => {
                if self.value < 1000.0 {
                    write!(f, "{freq:.0} Hz", freq = self.value)
                } else if self.value < 10000.0 {
                    write!(f, "{k_freq:.2} kHz", k_freq = self.value / 1000.0)
                } else {
                    write!(f, "{k_freq:.1} kHz", k_freq = self.value / 1000.0)
                }
            }
            ParamType::Duration => match self.value {
                v if v < 0.01 => write!(f, "{msec:.1}ms", msec = v * 1000.0),
                v if v < 1.0 => write!(f, "{msec:.0}ms", msec = v * 1000.0),
                v => write!(f, "{sec:.1}s", sec = v),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn format_float() {
        assert_eq!(ParamType::Float.formatted(0.1).to_string(), "0.1");
    }

    #[test]
    fn format_bool() {
        assert_eq!(ParamType::Bool.formatted(0.49).to_string(), "off");
        assert_eq!(ParamType::Bool.formatted(0.5).to_string(), "on");
    }

    #[test]
    fn format_decibel() {
        assert_eq!(ParamType::Decibel.formatted(1.0).to_string(), "0.00 dB");
        assert_eq!(ParamType::Decibel.formatted(2.0).to_string(), "6.02 dB");
        assert_eq!(ParamType::Decibel.formatted(0.5).to_string(), "-6.02 dB");
        assert_eq!(ParamType::Decibel.formatted(0.0125).to_string(), "-38.1 dB");
    }

    #[test]
    fn format_percent() {
        assert_eq!(ParamType::Percent.formatted(0.00).to_string(), "0.0%");
        assert_eq!(ParamType::Percent.formatted(0.01).to_string(), "1.0%");
        assert_eq!(ParamType::Percent.formatted(0.50).to_string(), "50.0%");
        assert_eq!(ParamType::Percent.formatted(1.00).to_string(), "100.0%");
        assert_eq!(ParamType::Percent.formatted(4.00).to_string(), "400.0%");
        assert_eq!(ParamType::Percent.formatted(-1.00).to_string(), "-100.0%");
    }

    #[test]
    fn format_frequency() {
        assert_eq!(ParamType::Frequency.formatted(0.4).to_string(), "0 Hz");
        assert_eq!(ParamType::Frequency.formatted(15.4).to_string(), "15 Hz");
        assert_eq!(ParamType::Frequency.formatted(150.4).to_string(), "150 Hz");
        assert_eq!(
            ParamType::Frequency.formatted(1500.4).to_string(),
            "1.50 kHz"
        );
        assert_eq!(
            ParamType::Frequency.formatted(15000.4).to_string(),
            "15.0 kHz"
        );
    }

    #[test]
    fn format_duration() {
        assert_eq!(ParamType::Duration.formatted(0.0014).to_string(), "1.4ms");
        assert_eq!(ParamType::Duration.formatted(0.0154).to_string(), "15ms");
        assert_eq!(ParamType::Duration.formatted(0.1554).to_string(), "155ms");
        assert_eq!(ParamType::Duration.formatted(1.5554).to_string(), "1.6s");
        assert_eq!(ParamType::Duration.formatted(10.5554).to_string(), "10.6s");
    }
}
