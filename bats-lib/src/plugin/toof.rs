use bats_dsp::{
    envelope::{
        ExponentialEnvelope, ExponentialEnvelopeParams, LinearEnvelope, LinearEnvelopeParams,
    },
    moog_filter::MoogFilter,
    sample_rate::SampleRate,
    waveform::{Sawtooth, Waveform},
};
use bmidi::{MidiMessage, Note, U7};
use enum_iterator::Sequence;

use super::{metadata::ParamType, BatsInstrument, BatsPluginParam, PluginParamMetadata};

/// A simple Sawtooth plugin.
#[derive(Debug, Clone, PartialEq)]
pub struct Toof {
    /// The velocity sensitivity.
    velocity_sensitivity: f32,
    /// The sample rate.
    sample_rate: SampleRate,
    /// Parameters for envelope.
    amp_envelope: ExponentialEnvelopeParams,
    /// The current filter frequency.
    filter_frequency: f32,
    /// The filter envelope.
    filter_envelope: LinearEnvelopeParams,
    /// The amount that the filter envelope affects the filter frequency.
    filter_envelope_amount: f32,
    /// The filter cutoff frequency.
    base_filter_cutoff: f32,
    /// The filter resonance.
    filter_resonance: f32,
    /// The active voices for toof.
    voice: ToofVoice,
}

/// The tweakable parameters for the toof plugin.
#[derive(Copy, Clone, Debug, Sequence)]
pub enum ToofParam {
    /// The velocity sensitivity.
    VelocitySensitivity = 1,
    /// The attack duration.
    Attack = 2,
    /// The decay duration.
    Decay = 3,
    /// The sustain amplitude.
    Sustain = 4,
    /// The release duration.
    Release = 5,
    /// The filter cuttoff.
    FilterCutoff = 6,
    /// The filter resonance.
    FilterResonance = 7,
    /// The amount that the filter envelope affects the filter.
    FilterEnvelopeAmount = 8,
    /// The attack of the filter envelope.
    FilterAttack = 9,
    /// The decay of the filter envelope.
    FilterDecay = 10,
    /// The sustain of the filter envelope.
    FilterSustain = 11,
    /// The release of the filter envelope.
    FilterRelease = 12,
}

impl BatsPluginParam for ToofParam {
    fn metadata(self) -> super::PluginParamMetadata {
        match self {
            ToofParam::VelocitySensitivity => PluginParamMetadata {
                id: self as u32,
                name: "velocity sensitivy",
                param_type: ParamType::Percent,
                default_value: 0.75,
                min_value: 0.01,
                max_value: 1.0,
            },
            ToofParam::Attack => PluginParamMetadata {
                id: self as u32,
                name: "attack",
                param_type: ParamType::Duration,
                default_value: 0.01,
                min_value: 0.001,
                max_value: 2.0,
            },
            ToofParam::Decay => PluginParamMetadata {
                id: self as u32,
                name: "decay",
                param_type: ParamType::Duration,
                default_value: 1.0,
                min_value: 0.001,
                max_value: 2.0,
            },
            ToofParam::Sustain => PluginParamMetadata {
                id: self as u32,
                name: "sustain",
                param_type: ParamType::Decibel,
                default_value: 1.0,
                min_value: 0.001,
                max_value: 1.0,
            },
            ToofParam::Release => PluginParamMetadata {
                id: self as u32,
                name: "release",
                param_type: ParamType::Duration,
                default_value: 0.1,
                min_value: 0.003,
                max_value: 2.0,
            },
            ToofParam::FilterCutoff => PluginParamMetadata {
                id: self as u32,
                name: "filter cutoff",
                param_type: ParamType::Frequency,
                default_value: MoogFilter::DEFAULT_FREQUENCY_CUTOFF,
                min_value: 50.0,
                max_value: 9000.0,
            },
            ToofParam::FilterResonance => PluginParamMetadata {
                id: self as u32,
                name: "filter resonance",
                param_type: ParamType::Percent,
                default_value: MoogFilter::DEFAULT_RESONANCE,
                min_value: 0.01,
                max_value: 0.65,
            },
            ToofParam::FilterEnvelopeAmount => PluginParamMetadata {
                id: self as u32,
                name: "filter envelope amount",
                param_type: ParamType::Percent,
                default_value: 0.01,
                min_value: 0.001,
                max_value: 2.0,
            },
            ToofParam::FilterAttack => PluginParamMetadata {
                id: self as u32,
                name: "filter attack",
                param_type: ParamType::Duration,
                default_value: 0.01,
                min_value: 0.001,
                max_value: 2.0,
            },
            ToofParam::FilterDecay => PluginParamMetadata {
                id: self as u32,
                name: "filter decay",
                param_type: ParamType::Duration,
                default_value: 1.0,
                min_value: 0.001,
                max_value: 2.0,
            },
            ToofParam::FilterSustain => PluginParamMetadata {
                id: self as u32,
                name: "filter sustain",
                param_type: ParamType::Percent,
                default_value: 1.0,
                min_value: 0.001,
                max_value: 1.0,
            },
            ToofParam::FilterRelease => PluginParamMetadata {
                id: self as u32,
                name: "filter release",
                param_type: ParamType::Duration,
                default_value: 0.1,
                min_value: 0.003,
                max_value: 2.0,
            },
        }
    }
}

/// A single voice for the Toof plugin. Each voice contains a single
/// note.
#[derive(Copy, Clone, Debug, PartialEq)]
struct ToofVoice {
    /// The midi note for the voice.
    note: Note,
    /// The sawtooth wave.
    wave: Sawtooth,
    /// The envelope.
    envelope: ExponentialEnvelope,
    /// The low pass filter.
    filter: MoogFilter,
    /// The filter envelope.
    filter_envelope: LinearEnvelope,
    /// The volume of this voice.
    volume: f32,
    /// The amount of presses.
    presses: u8,
}

impl Toof {
    /// Create a new Toof plugin with the given sample rate.
    pub fn new(sample_rate: SampleRate) -> Box<Toof> {
        let envelope = ExponentialEnvelopeParams::new(sample_rate, 0.005, 0.08, 0.4, 0.05);
        let mut voice = ToofVoice::new(sample_rate, MoogFilter::new(sample_rate), Note::C4, 1.0);
        voice.envelope = ExponentialEnvelope::INACTIVE;
        let base_filter_cutoff = MoogFilter::DEFAULT_FREQUENCY_CUTOFF;
        let filter_frequency = base_filter_cutoff;
        let filter_envelope = LinearEnvelopeParams::new(sample_rate, 0.2, 0.5, 0.2, 0.5);
        Box::new(Toof {
            velocity_sensitivity: 0.75,
            sample_rate,
            amp_envelope: envelope,
            filter_frequency,
            filter_envelope,
            filter_envelope_amount: ToofParam::FilterEnvelopeAmount.metadata().default_value,
            base_filter_cutoff,
            filter_resonance: MoogFilter::DEFAULT_RESONANCE,
            voice,
        })
    }

    fn velocity_to_volume(&self, velocity: U7) -> f32 {
        let velocity = u8::from(velocity) as f32 / u8::from(U7::MAX) as f32;
        velocity * self.velocity_sensitivity + (1.0 - self.velocity_sensitivity)
    }
}

impl BatsInstrument for Toof {
    const NAME: &'static str = "toof";
    type Param = ToofParam;

    /// Handle the processing and output to a single audio output.
    fn process(&mut self) -> (f32, f32) {
        let wave_amp = self.voice.wave.next_sample();
        let env_amp = self.voice.envelope.next_sample(&self.amp_envelope);
        let raw = self.voice.volume * wave_amp * env_amp;

        let filter_amp = (1.0 + self.filter_envelope_amount)
            * self
                .voice
                .filter_envelope
                .next_sample(&self.filter_envelope);
        self.voice.filter.set_cutoff_as_ratio(
            self.sample_rate
                .normalized_frequency(self.base_filter_cutoff * filter_amp)
                .min(0.5),
            self.filter_resonance,
        );
        let v = self.voice.filter.process(raw);
        (v, v)
    }

    /// Handle a midi event.
    fn handle_midi(&mut self, msg: &MidiMessage) {
        match msg {
            MidiMessage::NoteOff(_, note, _) | MidiMessage::NoteOn(_, note, U7::MIN) => {
                if self.voice.note == *note {
                    self.voice.presses -= 1;
                    if self.voice.presses == 0 {
                        self.voice.envelope.release();
                    }
                }
            }
            MidiMessage::NoteOn(_, note, velocity) => {
                let volume = self.velocity_to_volume(*velocity);
                self.voice.set_note(self.sample_rate, *note, volume);
            }
            MidiMessage::Reset => {
                self.voice.filter_envelope = LinearEnvelope::INACTIVE;
                self.voice.envelope = ExponentialEnvelope::INACTIVE;
            }
            _ => (),
        }
    }

    /// Get the value of a parameter.
    fn param(&self, param: ToofParam) -> f32 {
        match param {
            ToofParam::FilterCutoff => self.base_filter_cutoff,
            ToofParam::FilterResonance => self.filter_resonance,
            ToofParam::VelocitySensitivity => self.velocity_sensitivity,
            ToofParam::Attack => self.amp_envelope.attack(self.sample_rate),
            ToofParam::Decay => self.amp_envelope.decay(self.sample_rate),
            ToofParam::Sustain => self.amp_envelope.sustain(),
            ToofParam::Release => self.amp_envelope.release(self.sample_rate),
            ToofParam::FilterEnvelopeAmount => self.filter_envelope_amount,
            ToofParam::FilterAttack => self.filter_envelope.attack(self.sample_rate),
            ToofParam::FilterDecay => self.filter_envelope.decay(self.sample_rate),
            ToofParam::FilterSustain => self.filter_envelope.sustain(),
            ToofParam::FilterRelease => self.filter_envelope.release(self.sample_rate),
        }
    }

    /// Set a parameter.
    fn set_param(&mut self, param: ToofParam, value: f32) {
        match param {
            ToofParam::FilterCutoff => {
                self.base_filter_cutoff = value;
                self.voice.filter.set_cutoff(
                    self.sample_rate,
                    self.base_filter_cutoff,
                    self.filter_resonance,
                );
            }
            ToofParam::FilterResonance => {
                self.filter_resonance = value;
                self.voice.filter.set_cutoff(
                    self.sample_rate,
                    self.base_filter_cutoff,
                    self.filter_resonance,
                );
            }
            ToofParam::VelocitySensitivity => self.velocity_sensitivity = value,
            ToofParam::Attack => self.amp_envelope.set_attack(self.sample_rate, value),
            ToofParam::Decay => self.amp_envelope.set_decay(self.sample_rate, value),
            ToofParam::Sustain => self.amp_envelope.set_sustain(value),
            ToofParam::Release => self.amp_envelope.set_release(self.sample_rate, value),
            ToofParam::FilterEnvelopeAmount => self.filter_envelope_amount = value,
            ToofParam::FilterAttack => self.filter_envelope.set_attack(self.sample_rate, value),
            ToofParam::FilterDecay => self.filter_envelope.set_decay(self.sample_rate, value),
            ToofParam::FilterSustain => self.filter_envelope.set_sustain(self.sample_rate, value),
            ToofParam::FilterRelease => self.filter_envelope.set_release(self.sample_rate, value),
        }
    }
}

impl ToofVoice {
    /// Create a new Toof voice.
    fn new(sample_rate: SampleRate, filter: MoogFilter, note: Note, volume: f32) -> ToofVoice {
        ToofVoice {
            note,
            wave: Sawtooth::new(sample_rate, note.to_freq_f32()),
            envelope: ExponentialEnvelope::new(),
            filter,
            filter_envelope: LinearEnvelope::new(),
            volume,
            presses: 1,
        }
    }

    /// Set a new note for the current voice.
    fn set_note(&mut self, sample_rate: SampleRate, note: Note, volume: f32) {
        if self.note == note {
            self.presses += 1;
        } else {
            self.note = note;
            self.presses = 1;
            self.wave.set_frequency(sample_rate, note.to_freq_f32());
        }
        self.envelope = ExponentialEnvelope::new();
        self.filter_envelope = LinearEnvelope::new();
        self.volume = volume;
    }
}

#[cfg(test)]
mod tests {
    use bats_dsp::buffers::Buffers;
    use bmidi::{Channel, MidiMessage, Note, U7};
    use pretty_assertions::assert_eq;

    use super::*;

    fn approx_eq(a: f32, b: f32) -> bool {
        let diff_base = match (a == 0.0, b == 0.0) {
            (true, true) => return true,
            (true, false) => b.abs(),
            _ => a.abs(),
        };
        let diff_ratio = (a - b).abs() / diff_base;
        // Within 0.5% of the base value.
        diff_ratio < 0.005
    }

    #[test]
    fn note_press_produces_audio() {
        let buffer_size = 1024;
        let mut s = Toof::new(SampleRate::new(44100.0));
        let buffers = s.process_to_buffers(buffer_size, &[]);
        assert_eq!(buffers, Buffers::new(buffer_size),);

        let buffers = s.process_to_buffers(
            buffer_size,
            &[(0, MidiMessage::NoteOn(Channel::Ch1, Note::C3, U7::MAX))],
        );
        assert_ne!(buffers.left, vec![0f32; buffer_size]);
        assert_ne!(buffers.right, vec![0f32; buffer_size]);
    }

    #[test]
    fn key_presses_produces_monophonic_sound() {
        let note_a = (0, MidiMessage::NoteOn(Channel::Ch1, Note::A3, U7::MAX));
        let note_b = (0, MidiMessage::NoteOn(Channel::Ch1, Note::B4, U7::MAX));
        let toof = Toof::new(SampleRate::new(44100.0));

        // Playing note a then b should be equivalent to playing just note b.
        let single = toof.clone().process_to_buffers(100, &[note_b.clone()]);
        let both_notes = toof.clone().process_to_buffers(100, &[note_a, note_b]);
        assert_eq!(both_notes, single);
    }

    #[test]
    fn set_params_matches_get_params_values() {
        for param in ToofParam::all() {
            let mut toof = Toof::new(SampleRate::new(44100.0));
            let initial_value = toof.param(param);
            toof.set_param(param, initial_value);
            assert_eq!(toof.param(param), initial_value);
        }
    }

    #[test]
    fn set_params_can_set_to_min_and_max() {
        let params = ToofParam::all();
        for param in params {
            let mut toof = Toof::new(SampleRate::new(44100.0));

            toof.set_param(param, param.metadata().min_value);
            let got = toof.param(param);
            let want = param.metadata().min_value;
            assert!(approx_eq(got, want), "{got} == {want}, {param:?}");

            toof.set_param(param, param.metadata().max_value);
            let got = toof.param(param);
            let want = param.metadata().max_value;
            assert!(approx_eq(got, want), "{got} == {want}, {param:?}");
        }
    }

    #[test]
    fn can_set_param_for_unknown_id() {
        let final_param_id = ToofParam::all()
            .map(|p| p.metadata().id + 1)
            .max()
            .unwrap_or(u32::MAX);
        let mut toof = Toof::new(SampleRate::new(44100.0));
        let res = toof.set_param_by_id(final_param_id, 0.0);
        assert!(res.is_err());
    }

    #[test]
    fn bool_params_have_proper_ranges() {
        for param in ToofParam::all() {
            let metadata = param.metadata();
            if metadata.param_type != ParamType::Bool {
                continue;
            }
            // Bools must be within the small range to work well with the UI.
            let min_to_max_range = metadata.min_value..metadata.max_value;
            assert_eq!(min_to_max_range, (0.49..0.51));
            assert!(
                min_to_max_range.contains(&metadata.default_value),
                "{min_to_max_range:?}.contains(&{default:?})",
                default = metadata.default_value
            );
        }
    }

    #[test]
    fn play_and_release_notes() {
        let mut toof = Toof::new(SampleRate::new(44100.0));
        let midi_messages = [
            (1, MidiMessage::NoteOn(Channel::Ch1, Note::C3, U7::MAX)),
            (2, MidiMessage::NoteOff(Channel::Ch1, Note::C3, U7::MIN)),
            (10, MidiMessage::NoteOn(Channel::Ch1, Note::D3, U7::MAX)),
            (10, MidiMessage::NoteOn(Channel::Ch1, Note::D3, U7::MIN)),
            (20, MidiMessage::NoteOn(Channel::Ch1, Note::E3, U7::MAX)),
            (25, MidiMessage::NoteOn(Channel::Ch1, Note::E4, U7::MAX)),
            (44000, MidiMessage::Reset),
        ];
        let buffers = toof.process_to_buffers(44100, &midi_messages);
        assert_eq!(buffers.len(), 44100);
        assert_eq!(buffers.left, buffers.right);
    }
}
