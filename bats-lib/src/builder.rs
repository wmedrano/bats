use std::collections::HashMap;

use bats_dsp::buffers::Buffers;
use bats_dsp::sample_rate::SampleRate;
use enum_iterator::Sequence;
use serde::{Deserialize, Serialize};

use crate::any_plugin::AnyPlugin;
use crate::plugin::{empty::Empty, toof::Toof};
use crate::plugin::{BatsInstrument, MidiEvent};
use crate::track::Track;
use crate::transport::Transport;
use crate::Bats;

/// Creates a bats builder.
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct BatsBuilder {
    /// The sample rate.
    pub sample_rate: SampleRate,
    /// The buffer size.
    pub buffer_size: usize,
    /// The bpm.
    pub bpm: f32,
    /// The builders for the tracks.
    pub tracks: [TrackBuilder; Bats::SUPPORTED_TRACKS],
}

/// Creates a track.
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct TrackBuilder {
    /// The plugin builder.
    pub plugin: PluginBuilder,
    /// The volume for the track.
    pub volume: f32,
    /// The sequence for the track.
    pub sequence: Vec<MidiEvent>,
}

/// An object that is used to build plugins.
#[derive(Clone, Default, Debug, Serialize, Deserialize, PartialEq)]
pub struct PluginBuilder {
    /// The plugin identifier.
    plugin: PluginIdentifier,
    /// The parameters for the plugin.
    params: HashMap<u32, f32>,
}

#[derive(Copy, Clone, Default, Debug, PartialEq, Eq, Serialize, Deserialize, Sequence)]
pub enum PluginIdentifier {
    /// An empty plugin.
    #[default]
    Empty,
    /// The toof plugin.
    Toof,
}

impl Default for BatsBuilder {
    fn default() -> Self {
        BatsBuilder {
            sample_rate: SampleRate::new(44100.0),
            buffer_size: 256,
            bpm: 120.0,
            tracks: Default::default(),
        }
    }
}

impl BatsBuilder {
    /// Build the bats object.
    pub fn build(&self) -> Bats {
        Bats {
            transport: Transport::new(self.sample_rate, self.buffer_size, self.bpm),
            armed_track: 0,
            playback_enabled: true,
            recording_enabled: false,
            sample_rate: self.sample_rate,
            buffer_size: self.buffer_size,
            midi_buffer: Vec::with_capacity(self.buffer_size * 8),
            tracks: core::array::from_fn(|idx| {
                self.tracks[idx].build(self.sample_rate, self.buffer_size)
            }),
        }
    }

    /// Create a builder from a bats object.
    pub fn from_bats(b: &Bats) -> BatsBuilder {
        BatsBuilder {
            sample_rate: b.sample_rate,
            buffer_size: b.buffer_size,
            bpm: b.transport.bpm(),
            tracks: core::array::from_fn(|idx| {
                let track = &b.tracks[idx];
                TrackBuilder::from_bats(track)
            }),
        }
    }
}

impl TrackBuilder {
    /// Build the track.
    pub fn build(&self, sample_rate: SampleRate, buffer_size: usize) -> Track {
        let plugin = self.plugin.build(sample_rate);
        let mut sequence = Vec::with_capacity(Track::SEQUENCE_CAPACITY);
        sequence.extend_from_slice(&self.sequence);
        Track {
            plugin,
            volume: self.volume,
            sequence,
            output: Buffers::new(buffer_size),
        }
    }

    /// Create a track builder from a track.
    pub fn from_bats(t: &Track) -> TrackBuilder {
        TrackBuilder {
            plugin: PluginBuilder::from_bats(&t.plugin),
            volume: t.volume,
            sequence: t.sequence.clone(),
        }
    }
}

impl PluginBuilder {
    /// Iterate over the default plugins.
    pub fn iter_default_plugins() -> impl Iterator<Item = PluginBuilder> {
        enum_iterator::all::<PluginIdentifier>().map(|plugin| PluginBuilder {
            plugin,
            params: HashMap::new(),
        })
    }

    /// Get a plugin builder from a name.
    pub fn from_name_and_params(name: &str, params: HashMap<u32, f32>) -> Option<PluginBuilder> {
        let plugin = PluginIdentifier::from_name(name)?;
        Some(PluginBuilder { plugin, params })
    }

    /// The name of the plugin.
    pub const fn name(&self) -> &'static str {
        self.plugin.name()
    }

    /// Build the new plugin.
    pub fn build(&self, sample_rate: SampleRate) -> AnyPlugin {
        let mut p = match self.plugin {
            PluginIdentifier::Empty => AnyPlugin::Empty(Empty),
            PluginIdentifier::Toof => AnyPlugin::Toof(Toof::new(sample_rate)),
        };
        for (id, value) in self.params.iter() {
            p.set_param_by_id(*id, *value);
        }
        p
    }

    /// Create a plugin builder from an existing plugin.
    pub fn from_bats(p: &AnyPlugin) -> PluginBuilder {
        PluginBuilder {
            plugin: PluginIdentifier::from_plugin(p),
            params: p.param_id_to_value(),
        }
    }
}

impl PluginIdentifier {
    /// The name of the plugin.
    pub const fn name(self) -> &'static str {
        match self {
            PluginIdentifier::Empty => Empty::NAME,
            PluginIdentifier::Toof => Toof::NAME,
        }
    }

    /// Get the plugin indentifier from the name or `None` if there is no corresponding plugin.
    pub fn from_name(name: &str) -> Option<PluginIdentifier> {
        enum_iterator::all::<PluginIdentifier>().find(|i| i.name() == name)
    }

    /// Get the plugin identifier for the plugin.
    pub const fn from_plugin(p: &AnyPlugin) -> PluginIdentifier {
        match p {
            AnyPlugin::Empty(_) => PluginIdentifier::Empty,
            AnyPlugin::Toof(_) => PluginIdentifier::Toof,
        }
    }
}

impl Default for TrackBuilder {
    fn default() -> TrackBuilder {
        TrackBuilder {
            plugin: PluginBuilder::default(),
            volume: 1.0,
            sequence: Vec::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn build() {
        let initial_bats = {
            let mut b = BatsBuilder {
                sample_rate: SampleRate::new(44100.0),
                buffer_size: 256,
                bpm: 175.2,
                tracks: Default::default(),
            }
            .build();
            b.tracks[1].volume = 0.65;
            b.tracks[1].plugin = Toof::new(b.sample_rate).into();
            b
        };
        let initial_builder = BatsBuilder::from_bats(&initial_bats);
        let new_bats = initial_builder.build();
        let new_builder = BatsBuilder::from_bats(&new_bats);
        assert_eq!(initial_bats, new_bats);
        assert_eq!(initial_builder, new_builder);
    }

    #[test]
    fn builder_has_debug() {
        let b = BatsBuilder::default();
        let debug_text = format!("{:?}", b);
        assert!(!debug_text.is_empty());
    }

    #[test]
    fn can_build_all_default_plugins() {
        for plugin_builder in PluginBuilder::iter_default_plugins() {
            let _ = plugin_builder.build(SampleRate::new(44100.0));
        }
    }
}
