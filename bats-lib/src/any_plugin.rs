use std::collections::HashMap;

use bats_dsp::buffers::Buffers;
use bmidi::MidiMessage;
use log::warn;

use crate::plugin::{
    empty::Empty, metadata::PluginParamMetadata, toof::Toof, BatsInstrument, BatsPluginParam,
};

/// Contains all the plugins.
#[derive(Clone, Debug, PartialEq)]
pub enum AnyPlugin {
    /// The empty plugin.
    Empty(Empty),
    /// The toof plugin.
    Toof(Box<Toof>),
}

impl AnyPlugin {
    /// The name of the underlying plugin.
    pub fn name(&self) -> &'static str {
        match self {
            AnyPlugin::Empty(_) => Empty::NAME,
            AnyPlugin::Toof(_) => Toof::NAME,
        }
    }

    /// Get all the plugin params metadata for the underlying plugin.
    pub fn collect_param_metadata(&self) -> Vec<PluginParamMetadata> {
        match self {
            AnyPlugin::Empty(p) => p.iter_params().map(|p| p.metadata()).collect(),
            AnyPlugin::Toof(p) => p.iter_params().map(|p| p.metadata()).collect(),
        }
    }

    /// Process a batch of midi input and output to `audio_out`.
    pub fn process_batch(&mut self, midi_in: &[(u32, MidiMessage)], audio_out: &mut Buffers) {
        match self {
            AnyPlugin::Empty(p) => p.process_batch(midi_in, audio_out),
            AnyPlugin::Toof(p) => p.process_batch(midi_in, audio_out),
        }
    }

    /// Get the value of a parameter by its id.
    pub fn param_by_id(&self, id: u32) -> Option<f32> {
        match self {
            AnyPlugin::Empty(p) => p.param_by_id(id),
            AnyPlugin::Toof(p) => p.param_by_id(id),
        }
    }

    /// Set the parameter by the id. If the id is not valid for the plugin, then nothing happens.
    pub fn set_param_by_id(&mut self, id: u32, value: f32) {
        let err = match self {
            AnyPlugin::Empty(p) => p.set_param_by_id(id, value),
            AnyPlugin::Toof(p) => p.set_param_by_id(id, value),
        };
        if let Err(err) = err {
            warn!("{err}")
        }
    }

    /// Get the map from `param_id` to the parameter value.
    pub fn param_id_to_value(&self) -> HashMap<u32, f32> {
        match self {
            AnyPlugin::Empty(p) => p.param_id_to_value(),
            AnyPlugin::Toof(p) => p.param_id_to_value(),
        }
    }

    /// Reset the plugin midi state.
    pub fn reset_midi(&mut self) {
        match self {
            AnyPlugin::Empty(p) => p.handle_midi(&MidiMessage::Reset),
            AnyPlugin::Toof(p) => p.handle_midi(&MidiMessage::Reset),
        }
    }
}

impl From<Empty> for AnyPlugin {
    fn from(v: Empty) -> AnyPlugin {
        AnyPlugin::Empty(v)
    }
}

impl From<Box<Toof>> for AnyPlugin {
    fn from(v: Box<Toof>) -> AnyPlugin {
        AnyPlugin::Toof(v)
    }
}

impl Default for AnyPlugin {
    fn default() -> AnyPlugin {
        AnyPlugin::Empty(Empty)
    }
}

#[cfg(test)]
mod tests {
    use bats_dsp::sample_rate::SampleRate;

    use crate::plugin::toof::ToofParam;

    use super::*;

    #[test]
    fn can_get_param_value_by_id() {
        let mut p: AnyPlugin = Toof::new(SampleRate::new(44100.0)).into();
        // Using sustain as the parameter since it is not prone to precision loss due to floating
        // points.
        let param_id = ToofParam::Sustain.metadata().id;
        p.set_param_by_id(param_id, 0.25);
        assert_eq!(p.param_by_id(param_id), Some(0.25));
    }

    #[test]
    fn param_value_by_id_on_bad_id_returns_none() {
        let mut p: AnyPlugin = Toof::new(SampleRate::new(44100.0)).into();
        let param_id: u32 = ToofParam::all().map(|p| p.metadata().id + 1).max().unwrap();
        p.set_param_by_id(param_id, 0.25);
        assert_eq!(p.param_by_id(param_id), None);
    }

    #[test]
    fn collect_param_by_metadata_returns_all_metadata() {
        let p: AnyPlugin = Toof::new(SampleRate::new(44100.0)).into();
        assert_eq!(
            p.collect_param_metadata(),
            ToofParam::all().map(|p| p.metadata()).collect::<Vec<_>>()
        );
    }
}
