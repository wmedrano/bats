use std::collections::HashMap;

use anyhow::bail;
use bats_dsp::{buffers::Buffers, position::Position};
use bmidi::MidiMessage;
use enum_iterator::Sequence;
use serde::{Deserialize, Serialize};

use self::metadata::PluginParamMetadata;

pub mod empty;
pub mod metadata;
pub mod toof;

/// Contains a midi event along with its `Position` timestamp.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct MidiEvent {
    /// The position of the midi event.
    pub position: Position,
    /// The midi event.
    pub midi: MidiMessage,
}

pub trait BatsPluginParam: Copy + Clone + Sequence {
    /// Get the metadata for the plugin param.
    fn metadata(self) -> PluginParamMetadata;

    /// Convert an id into the param or `None` if it is not a valid id.
    fn from_id(id: u32) -> Option<Self> {
        enum_iterator::all::<Self>().find(|p| p.metadata().id == id)
    }

    /// Return an iterator over all the params.
    fn all() -> enum_iterator::All<Self> {
        enum_iterator::all()
    }
}

/// Defines a generic instrument plugin.
pub trait BatsInstrument {
    const NAME: &'static str;
    type Param: BatsPluginParam;

    /// Handle a midi message.
    fn handle_midi(&mut self, msg: &MidiMessage);

    /// Produce the next samples in the frame.
    fn process(&mut self) -> (f32, f32);

    /// Get the value of the parameter.
    fn param(&self, _: Self::Param) -> f32 {
        0.0
    }

    /// Set a parameter.
    fn set_param(&mut self, _param: Self::Param, _value: f32) {}

    /// Run any batch cleanup operations.
    fn batch_cleanup(&mut self) {}

    /// Handle processing of `midi_in` and output to `left_out` and
    /// `right_out`.
    ///
    /// Prefer using this default behavior unless benchmarking shows significant performance
    /// improvements.
    fn process_batch(&mut self, midi_in: &[(u32, MidiMessage)], output: &mut Buffers) {
        let sample_count = output.len();
        let mut midi_iter = midi_in.iter().peekable();
        for i in 0..sample_count {
            while let Some((_, msg)) = midi_iter.next_if(|(frame, _)| *frame <= i as u32) {
                self.handle_midi(msg);
            }
            output.set(i, self.process())
        }
        self.batch_cleanup();
    }

    /// Handle processing of `midi_in` and return the results. This is
    /// often less efficient but is included for less performance
    /// critical use cases like unit tests.
    fn process_to_buffers(
        &mut self,
        sample_count: usize,
        midi_in: &[(u32, MidiMessage)],
    ) -> Buffers {
        let mut buffers = Buffers::new(sample_count);
        self.process_batch(midi_in, &mut buffers);
        buffers
    }

    /// Set a parameter given its id.
    fn set_param_by_id(&mut self, id: u32, value: f32) -> anyhow::Result<()> {
        let param = match Self::Param::from_id(id) {
            Some(p) => p,
            None => bail!(
                "could not convert param id {id} to param for plugin {plugin_name}",
                plugin_name = Self::NAME
            ),
        };
        self.set_param(param, value);
        Ok(())
    }

    /// Gets a map from param id to its value.
    fn param_id_to_value(&self) -> HashMap<u32, f32> {
        enum_iterator::all::<Self::Param>()
            .map(|p: Self::Param| (p.metadata().id, self.param(p)))
            .collect()
    }

    // Get the value of a param given its id.
    fn param_by_id(&self, id: u32) -> Option<f32> {
        let param = enum_iterator::all::<Self::Param>().find(|p| p.metadata().id == id)?;
        Some(self.param(param))
    }

    /// Iterate through all params for the plugin.
    fn iter_params(&self) -> enum_iterator::All<Self::Param> {
        enum_iterator::all()
    }
}
