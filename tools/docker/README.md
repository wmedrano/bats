Docker
======

Docker image that is used for CI tests.

Release
-------

```shell
cd tools/docker
docker login registry.gitlab.com --username <username> --password <password or token>
docker build -t registry.gitlab.com/wmedrano/bats .
docker push registry.gitlab.com/wmedrano/bats
```
