use std::{io::Stdout, sync::Arc};

use anyhow::Result;
use bats_async::{channels::CommandSender, BatsState};
use bats_lib::{
    builder::PluginBuilder,
    plugin::metadata::{ParamType, PluginParamMetadata},
    Bats,
};
use events::EventPoll;
use log::{error, info, warn};
use menu::{Menu, MenuAction, SelectorMenu};
use ratatui::{prelude::CrosstermBackend, Terminal};

pub mod events;
pub mod menu;
pub mod selector;

/// Runs the Ui.
pub struct Ui {
    /// The backing terminal.
    terminal: Terminal<CrosstermBackend<Stdout>>,
    /// The object to poll events from.
    event_poll: EventPoll,
    /// Contains bats related state information.
    bats_state: Arc<BatsState>,
    /// The file picker.
    file_picker: SelectorMenu<'static, String, fn(&String) -> String, Vec<String>>,
}

impl Ui {
    /// Create a new `Ui`.
    pub fn new(bats: &Bats, commands: CommandSender) -> Result<Ui> {
        let bats_state = Arc::new(BatsState::new(bats, commands));
        // Initialize the terminal user interface.
        let backend = CrosstermBackend::new(std::io::stdout());
        let mut terminal = Terminal::new(backend)?;
        crossterm::terminal::enable_raw_mode()?;
        crossterm::execute!(
            std::io::stdout(),
            crossterm::terminal::EnterAlternateScreen,
            crossterm::event::EnableMouseCapture
        )?;
        terminal.hide_cursor()?;
        terminal.clear()?;
        info!("Initialized UI.");
        Ok(Ui {
            terminal,
            event_poll: EventPoll {},
            bats_state,
            file_picker: SelectorMenu::new(
                "File".to_string(),
                (1..9)
                    .map(|n| format!("project_{n}.bats"))
                    .collect::<Vec<_>>(),
                |i: &String| i.clone(),
            ),
        })
    }

    /// Run the UI.
    pub fn run(&mut self) -> Result<()> {
        #[derive(Copy, Clone)]
        enum MainMenuItem {
            Tracks,
            Sequencing,
            Project,
            Quit,
        }
        let menu_items = [
            MainMenuItem::Tracks,
            MainMenuItem::Sequencing,
            MainMenuItem::Project,
            MainMenuItem::Quit,
        ];
        let mut menu = SelectorMenu::new(
            "Main".to_string(),
            &menu_items,
            |i: &MainMenuItem| match i {
                MainMenuItem::Tracks => "Tracks".to_string(),
                MainMenuItem::Sequencing => "Sequencing".to_string(),
                MainMenuItem::Project => "Project".to_string(),
                MainMenuItem::Quit => "Quit".to_string(),
            },
        );
        loop {
            match menu.run(&self.event_poll, &mut self.terminal)? {
                Some(MainMenuItem::Tracks) => self.run_tracks()?,
                Some(MainMenuItem::Sequencing) => self.run_sequencing()?,
                Some(MainMenuItem::Project) => self.run_project()?,
                Some(MainMenuItem::Quit) => return Ok(()),
                None => (),
            }
        }
    }

    /// Run the page for a single track. This has links to other pages for the track such as
    /// changing the plugin and adjusting the params.
    fn run_tracks(&mut self) -> Result<()> {
        #[derive(Copy, Clone)]
        enum TrackMenuItem {
            Track,
            ChangeVolume,
            ChangePlugin,
            Params,
        }
        let menu_items = [
            TrackMenuItem::Track,
            TrackMenuItem::ChangeVolume,
            TrackMenuItem::ChangePlugin,
            TrackMenuItem::Params,
        ];
        let mut menu =
            SelectorMenu::new("".to_string(), &menu_items, |i: &TrackMenuItem| match i {
                TrackMenuItem::Track => {
                    format!(
                        "Track: {title}",
                        title = self.bats_state.armed_track().title()
                    )
                }
                TrackMenuItem::ChangeVolume => {
                    format!(
                        "Volume: {volume}",
                        volume = ParamType::Decibel.formatted(self.bats_state.armed_track().volume)
                    )
                }
                TrackMenuItem::ChangePlugin => format!(
                    "Change Plugin ({plugin_name})",
                    plugin_name = self.bats_state.armed_track().title()
                ),
                TrackMenuItem::Params => "Params".to_string(),
            })
            .with_extra_event_handler(|event, action| match (action, event) {
                (TrackMenuItem::Track, events::Event::Left) => {
                    self.bats_state.scroll_armed(-1);
                    MenuAction::Redraw
                }
                (TrackMenuItem::Track, events::Event::Right) => {
                    self.bats_state.scroll_armed(1);
                    MenuAction::Redraw
                }
                (TrackMenuItem::ChangeVolume, events::Event::Left) => {
                    self.bats_state
                        .modify_track_volume(self.bats_state.armed(), |v| v.volume / 1.05);
                    MenuAction::Redraw
                }
                (TrackMenuItem::ChangeVolume, events::Event::Right) => {
                    self.bats_state
                        .modify_track_volume(self.bats_state.armed(), |v| v.volume * 1.05);
                    MenuAction::Redraw
                }
                _ => MenuAction::None,
            });
        loop {
            menu.set_title(format!("Track - {}", self.bats_state.armed_track().title()));
            let selected = match menu.run(&self.event_poll, &mut self.terminal)? {
                Some(s) => s,
                None => return Ok(()),
            };
            match selected {
                TrackMenuItem::Track => (),
                TrackMenuItem::ChangePlugin => {
                    if let Ok(Some(b)) = {
                        let track = self.bats_state.armed_track();
                        Self::select_plugin(
                            format!(
                                "Change Plugin for {title}({plugin})",
                                title = track.title(),
                                plugin = track.plugin_name,
                            ),
                            &self.event_poll,
                            &mut self.terminal,
                        )
                    } {
                        let plugin = b.build(self.bats_state.sample_rate());
                        self.bats_state.set_plugin(self.bats_state.armed(), plugin);
                    }
                }
                TrackMenuItem::ChangeVolume => (),
                TrackMenuItem::Params => {
                    Self::edit_params(&self.event_poll, &mut self.terminal, &self.bats_state)?
                }
            }
        }
    }

    /// Run the sequencing page.
    fn run_sequencing(&mut self) -> Result<()> {
        let bats_state = self.bats_state.clone();
        let min_metronome_volume = 2f32.powi(-10);
        #[derive(Copy, Clone)]
        enum Item {
            Bpm,
            Volume,
            Track,
            PlayStop,
            Recording,
            ClearTrackSequence,
            ClearAllSequences,
            Back,
        }
        let mut menu = SelectorMenu::new(
            "Sequencing".to_string(),
            [
                Item::Bpm,
                Item::Volume,
                Item::Track,
                Item::PlayStop,
                Item::Recording,
                Item::ClearTrackSequence,
                Item::ClearAllSequences,
                Item::Back,
            ],
            |i: &Item| match i {
                Item::Bpm => format!("BPM: {bpm}", bpm = bats_state.bpm()),
                Item::Volume => {
                    format!(
                        "Metronome Volume: {volume}",
                        volume = ParamType::Decibel.formatted(bats_state.metronome_volume())
                    )
                }
                Item::Track => {
                    format!("Track: {title}", title = bats_state.armed_track().title())
                }
                Item::PlayStop => {
                    let enabled = if bats_state.playback_enabled() {
                        1.0
                    } else {
                        0.0
                    };
                    format!(
                        "Playback: {enabled}",
                        enabled = ParamType::Bool.formatted(enabled)
                    )
                }
                Item::Recording => {
                    let enabled = if bats_state.recording_enabled() {
                        1.0
                    } else {
                        0.0
                    };
                    format!(
                        "Recording: {enabled}",
                        enabled = ParamType::Bool.formatted(enabled)
                    )
                }
                Item::ClearTrackSequence => "Clear Track Sequence".to_string(),
                Item::ClearAllSequences => "Clear All Sequences".to_string(),
                Item::Back => "Back".to_string(),
            },
        )
        .with_extra_event_handler(|event, selected| match (event, selected) {
            (events::Event::Left, Item::Volume) => {
                bats_state.modify_metronome_volume(|v| {
                    if v <= min_metronome_volume {
                        0.0
                    } else {
                        v / 2f32.sqrt()
                    }
                });
                MenuAction::Redraw
            }
            (events::Event::Right, Item::Volume) => {
                bats_state.modify_metronome_volume(|v| {
                    if v < min_metronome_volume {
                        min_metronome_volume
                    } else {
                        v * 2f32.sqrt()
                    }
                });
                MenuAction::Redraw
            }
            (events::Event::Left, Item::Bpm) => {
                bats_state.modify_bpm(|v| v - 1.0);
                MenuAction::Redraw
            }
            (events::Event::Right, Item::Bpm) => {
                bats_state.modify_bpm(|v| v + 1.0);
                MenuAction::Redraw
            }
            (events::Event::Left, Item::Track) => {
                bats_state.scroll_armed(-1);
                MenuAction::Redraw
            }
            (events::Event::Right, Item::Track) => {
                bats_state.scroll_armed(1);
                MenuAction::Redraw
            }
            (events::Event::Left, Item::PlayStop) => {
                bats_state.set_playback(false);
                MenuAction::Redraw
            }
            (events::Event::Right, Item::PlayStop) => {
                bats_state.set_playback(true);
                MenuAction::Redraw
            }
            (events::Event::Left, Item::Recording) => {
                bats_state.set_recording(false);
                MenuAction::Redraw
            }
            (events::Event::Right, Item::Recording) => {
                bats_state.set_recording(true);
                MenuAction::Redraw
            }
            _ => MenuAction::None,
        });
        while let Some(item) = menu.run(&self.event_poll, &mut self.terminal)? {
            match item {
                Item::Bpm => (),
                Item::Volume => (),
                Item::Track => self.run_tracks()?,
                Item::PlayStop => self.bats_state.toggle_playback(),
                Item::Recording => self.bats_state.toggle_recording(),
                Item::ClearTrackSequence => self
                    .bats_state
                    .set_sequence(self.bats_state.armed(), Vec::new()),
                Item::ClearAllSequences => {
                    for track_id in 0..Bats::SUPPORTED_TRACKS {
                        self.bats_state.set_sequence(track_id, Vec::new())
                    }
                }
                Item::Back => return Ok(()),
            }
        }
        Ok(())
    }

    /// Run the project page.
    fn run_project(&mut self) -> Result<()> {
        #[derive(Copy, Clone)]
        enum Item {
            Save,
            Load,
        }
        let mut menu = SelectorMenu::new(
            "Project".to_string(),
            [Item::Save, Item::Load],
            |i: &Item| match i {
                Item::Save => "Save".to_string(),
                Item::Load => "Load".to_string(),
            },
        );
        while let Some(item) = menu.run(&self.event_poll, &mut self.terminal)? {
            match self.file_picker.run(&self.event_poll, &mut self.terminal) {
                Ok(Some(fname)) => match item {
                    Item::Save => {
                        if let Err(err) = self.bats_state.save(&fname) {
                            error!("Failed to save to {fname}: {err}");
                        }
                    }
                    Item::Load => {
                        if let Err(err) = self.bats_state.load(&fname) {
                            error!("Failed to load from {fname}: {err}");
                        }
                    }
                },
                Ok(None) => (),
                Err(err) => error!("Failed to select filename: {err}"),
            }
        }
        Ok(())
    }

    /// Select a plugin and return it. If the selection is canceled, then `Ok(None)` is returned.
    fn select_plugin(
        title: String,
        event_poll: &EventPoll,
        terminal: &mut Terminal<CrosstermBackend<Stdout>>,
    ) -> Result<Option<PluginBuilder>> {
        let mut menu = SelectorMenu::new(
            title,
            PluginBuilder::iter_default_plugins().collect::<Vec<_>>(),
            |b: &PluginBuilder| b.name().to_string(),
        );
        menu.run(event_poll, terminal)
    }

    /// Edit the params for the track with `track_id`.
    fn edit_params(
        event_poll: &EventPoll,
        terminal: &mut Terminal<CrosstermBackend<Stdout>>,
        bats_state: &BatsState,
    ) -> Result<()> {
        let track = bats_state.armed_track();
        let title = format!("{} Params", track.title());
        let mut menu = SelectorMenu::new(title, &track.plugin_params, |p: &PluginParamMetadata| {
            let value = bats_state
                .track_by_id(track.id)
                .unwrap()
                .params
                .get(&p.id)
                .copied()
                .unwrap_or(0.0);
            format!(
                "{name}: {value}",
                name = p.name,
                value = p.param_type.formatted(value),
            )
        })
        .with_extra_event_handler(|event, param| match event {
            events::Event::Left => {
                bats_state.modify_param(track.id, param.id, |v| v / 1.05);
                MenuAction::Redraw
            }
            events::Event::Right => {
                bats_state.modify_param(track.id, param.id, |v| v * 1.05);
                MenuAction::Redraw
            }
            _ => MenuAction::None,
        });
        menu.run(event_poll, terminal)?;
        Ok(())
    }
}

impl Drop for Ui {
    fn drop(&mut self) {
        match crossterm::execute!(
            std::io::stdout(),
            crossterm::terminal::LeaveAlternateScreen,
            crossterm::event::DisableMouseCapture
        ) {
            Ok(()) => (),
            Err(err) => {
                warn!("Failed to leave terminal screen and disable terminal mouse capture: {err}")
            }
        }
        match crossterm::terminal::disable_raw_mode() {
            Ok(()) => (),
            Err(err) => warn!("Failed to disable raw mode: {err}"),
        }
    }
}
