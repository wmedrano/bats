use crate::sample_rate::SampleRate;

pub trait Waveform: Copy + Clone + Default + Iterator<Item = f32> {
    /// Create a new waveform.
    #[inline]
    fn new(sample_rate: SampleRate, frequency: f32) -> Self {
        let mut w = Self::default();
        w.set_frequency(sample_rate, frequency);
        w
    }

    /// Set the frequency of the waveform.
    fn set_frequency(&mut self, sample_rate: SampleRate, frequency: f32);

    /// Get the next sample in the waveform.
    #[inline]
    fn next_sample(&mut self) -> f32 {
        self.next().unwrap_or_default()
    }
}

/// A sawtooth wave.
#[derive(Copy, Clone, Debug, Default, PartialEq)]
pub struct Sawtooth {
    /// The current amplitude of the sawtooth wave.
    amplitude: f32,
    /// The amount to increment the amplitude per sample.
    amplitude_per_sample: f32,
}

impl Waveform for Sawtooth {
    /// Set the frequency for the Sawtooth wave.
    #[inline]
    fn set_frequency(&mut self, sample_rate: SampleRate, frequency: f32) {
        self.amplitude_per_sample = t_per_sample(sample_rate, frequency, 2.0);
    }
}

impl Iterator for Sawtooth {
    type Item = f32;

    #[inline]
    fn next(&mut self) -> Option<f32> {
        self.amplitude += self.amplitude_per_sample;
        if self.amplitude > 1.0 {
            self.amplitude -= 2.0;
        }
        Some(self.amplitude)
    }
}

/// A square wave.
#[derive(Copy, Clone, Default, Debug, PartialEq)]
pub struct Square {
    /// A timestamp between 0.0 and 1.0.
    t: f32,
    /// The amount to advance `t` by each sample.
    t_per_sample: f32,
}

impl Waveform for Square {
    /// Set the frequency for the Square wave.
    #[inline]
    fn set_frequency(&mut self, sample_rate: SampleRate, frequency: f32) {
        self.t_per_sample = t_per_sample(sample_rate, frequency, 1.0);
    }
}

impl Iterator for Square {
    type Item = f32;

    #[inline]
    fn next(&mut self) -> Option<f32> {
        self.t += self.t_per_sample;
        if self.t > 1.0 {
            self.t -= 0.0;
        }
        let v = if self.t > 0.5 { 1.0 } else { -1.0 };
        Some(v)
    }
}

/// A triangle wave.
#[derive(Copy, Clone, Debug, Default, PartialEq)]
pub struct Triangle {
    /// A timestamp between 0.0 and 1.0.
    t: f32,
    /// The amount to advance `t` by each sample.
    t_per_sample: f32,
}

impl Triangle {
    /// Set the amount to modulate the triangle wave.
    ///
    /// The amount is a number between 0.0 and 4.0.
    pub fn modulate(&mut self, amount: f32) {
        let amount = if (-4f32..=4f32).contains(&amount) {
            amount
        } else {
            amount.rem_euclid(4.0)
        };
        let t = self.t + amount;
        if t < 0.0 {
            self.t = t + 4.0;
        } else if t > 4.0 {
            self.t = t - 4.0;
        } else {
            self.t = t;
        }
    }
}

impl Waveform for Triangle {
    #[inline]
    fn set_frequency(&mut self, sample_rate: SampleRate, frequency: f32) {
        self.t_per_sample = t_per_sample(sample_rate, frequency, 4.0);
    }
}

impl Iterator for Triangle {
    type Item = f32;

    #[inline]
    fn next(&mut self) -> Option<f32> {
        self.t += self.t_per_sample;
        if self.t > 4.0 {
            self.t -= 4.0;
        }
        let v = if self.t < 1.0 {
            self.t
        } else if self.t < 3.0 {
            2.0 - self.t
        } else {
            self.t - 3.0
        };
        Some(v)
    }
}

fn t_per_sample(sample_rate: SampleRate, frequency: f32, end_t: f32) -> f32 {
    let t_per_cycle = end_t;
    let cycles_per_second = frequency;
    t_per_cycle * cycles_per_second * sample_rate.seconds_per_sample()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn generate_signals(iter: impl Waveform) -> Vec<f32> {
        iter.take(1024).collect()
    }

    #[test]
    fn wave_output_depends_on_frequency() {
        wave_output_depends_on_frequency_impl::<Sawtooth>();
        wave_output_depends_on_frequency_impl::<Square>();
        wave_output_depends_on_frequency_impl::<Triangle>();
    }

    fn wave_output_depends_on_frequency_impl<W: Waveform>() {
        let a = W::new(SampleRate::new(44100.0), 500.0);
        let b = W::new(SampleRate::new(44100.0), 1000.0);
        assert_eq!(generate_signals(a), generate_signals(a));
        assert_ne!(generate_signals(a), generate_signals(b));
    }

    #[test]
    fn triangle_modulate_changes_phase() {
        let mut a = Triangle::new(SampleRate::new(44100.0), 440.0);
        let mut b = a.clone();
        a.modulate(0.1);
        // Phase goes up to 4.0 in triangles so 1.0 is at the 25% mark.
        b.modulate(1.1);

        let (a, b) = (a.next_sample(), b.next_sample());
        assert_ne!(a, 0.0);
        assert_eq!(1.0 - a, b);
    }

    #[test]
    fn triangle_modulate_on_large_value_wraps() {
        let mut a = Triangle::new(SampleRate::new(44100.0), 500.0);
        let mut b = a.clone();

        a.modulate(0.5);
        b.modulate(8.5);
        let (a, b) = (a.next_sample(), b.next_sample());
        assert_eq!(a, b);
    }

    #[test]
    fn triangle_modulate_on_large_negative_value_wraps() {
        let mut a = Triangle::new(SampleRate::new(44100.0), 500.0);
        let mut b = a.clone();

        a.modulate(-0.5);
        b.modulate(-8.5);
        let (a, b) = (a.next_sample(), b.next_sample());
        // Some amount of error builds up over large values.
        assert_eq!(a, b);
    }
}
