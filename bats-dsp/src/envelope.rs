use serde::{Deserialize, Serialize};

use crate::sample_rate::SampleRate;

/// The parameters for an envelope.
#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct LinearEnvelopeParams {
    /// The amount of amp to add in each attack phase sample.
    attack_delta: f32,
    /// The amount of amp to add in each decay phase sample. This should be negative.
    decay_delta: f32,
    /// The amount of amp to add in each release sample. This should be negative.
    release_delta: f32,
    /// The amp level of the sustain phase.
    sustain_amp: f32,
    /// The decay in seconds. Required in cases where recomputation is needed and decay is not
    /// computable.
    decay_seconds: f32,
}

impl Default for LinearEnvelopeParams {
    /// Create a default instance of an envelope. The default sustains an amp at 1.0 immediately and
    /// cuts off immediately after release.
    fn default() -> LinearEnvelopeParams {
        LinearEnvelopeParams {
            attack_delta: 1.0,
            decay_delta: -1.0,
            release_delta: -1.0,
            sustain_amp: 1.0,
            decay_seconds: 0.0,
        }
    }
}

impl LinearEnvelopeParams {
    /// Create a new envelope params object.
    pub fn new(
        sample_rate: SampleRate,
        attack_seconds: f32,
        decay_seconds: f32,
        sustain_amp: f32,
        release_seconds: f32,
    ) -> LinearEnvelopeParams {
        let mut p = LinearEnvelopeParams::default();
        p.set_attack(sample_rate, attack_seconds);
        p.set_decay(sample_rate, decay_seconds);
        p.set_sustain(sample_rate, sustain_amp);
        p.set_release(sample_rate, release_seconds);
        p
    }

    /// Get the attack value in seconds.
    pub fn attack(&self, sample_rate: SampleRate) -> f32 {
        let attack_samples = 1.0 / self.attack_delta;
        attack_samples * sample_rate.seconds_per_sample()
    }

    /// Set the attack value.
    pub fn set_attack(&mut self, sample_rate: SampleRate, attack_seconds: f32) {
        debug_assert!(attack_seconds >= 0.0);
        if attack_seconds == 0.0 {
            self.attack_delta = 1.0;
        } else {
            let attack_frames = sample_rate.sample_rate() * attack_seconds;
            self.attack_delta = 1.0 / attack_frames;
        }
    }

    /// Get the decay value.
    pub fn decay(&self, _sample_rate: SampleRate) -> f32 {
        self.decay_seconds
    }

    /// Set the decay.
    pub fn set_decay(&mut self, sample_rate: SampleRate, decay_seconds: f32) {
        debug_assert!(decay_seconds >= 0.0);
        self.decay_seconds = decay_seconds;
        if decay_seconds == 0.0 || self.sustain_amp == 1.0 {
            self.decay_delta = -1.0;
        } else {
            let decay_frames = sample_rate.sample_rate() * decay_seconds;
            self.decay_delta = (self.sustain_amp - 1.0) / decay_frames;
        }
        debug_assert!(self.decay_delta < 0.0);
    }

    /// Returns the sustain of this [`EnvelopeParams`].
    pub fn sustain(&self) -> f32 {
        self.sustain_amp
    }

    /// Sets the sustain of this [`EnvelopeParams`].
    pub fn set_sustain(&mut self, sample_rate: SampleRate, sustain_amp: f32) {
        debug_assert!(
            (0.0..=1.0).contains(&sustain_amp),
            "0.0 <= {sustain_amp} <= 1.0"
        );
        self.sustain_amp = sustain_amp;
        self.set_decay(sample_rate, self.decay_seconds);
    }

    /// Get the release value in seconds.
    pub fn release(&self, sample_rate: SampleRate) -> f32 {
        let release_frames = -self.sustain_amp / self.release_delta;
        release_frames * sample_rate.seconds_per_sample()
    }

    /// Sets the release of this [`EnvelopeParams`].
    pub fn set_release(&mut self, sample_rate: SampleRate, release_seconds: f32) {
        debug_assert!(release_seconds >= 0.0);
        if release_seconds == 0.0 {
            self.release_delta = -1.0;
        } else {
            let release_frames = sample_rate.sample_rate() * release_seconds;
            self.release_delta = -self.sustain_amp / release_frames;
        }
    }
}

/// The parameters for an envelope.
#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ExponentialEnvelopeParams {
    /// The amount of amp to add in each attack phase sample.
    attack_multiplier: f32,
    /// The amount of amp to add in each decay phase sample. This should be negative.
    decay_multiplier: f32,
    /// The amount of amp to add in each release sample. This should be negative.
    release_multiplier: f32,
    /// The amp level of the sustain phase.
    sustain_amp: f32,
}

impl Default for ExponentialEnvelopeParams {
    /// Create a default instance of an envelope. The default sustains an amp at 1.0 immediately and
    /// cuts off immediately after release.
    fn default() -> ExponentialEnvelopeParams {
        ExponentialEnvelopeParams {
            attack_multiplier: 0.0,
            decay_multiplier: 0.0,
            release_multiplier: 0.0,
            sustain_amp: 1.0,
        }
    }
}

impl ExponentialEnvelopeParams {
    /// Create a new envelope params object.
    pub fn new(
        sample_rate: SampleRate,
        attack_seconds: f32,
        decay_seconds: f32,
        sustain_amp: f32,
        release_seconds: f32,
    ) -> ExponentialEnvelopeParams {
        let mut p = ExponentialEnvelopeParams::default();
        p.set_attack(sample_rate, attack_seconds);
        p.set_decay(sample_rate, decay_seconds);
        p.set_sustain(sustain_amp);
        p.set_release(sample_rate, release_seconds);
        p
    }

    /// Get the attack value in seconds.
    pub fn attack(&self, sample_rate: SampleRate) -> f32 {
        multiplier_to_duration(sample_rate, self.attack_multiplier)
    }

    /// Set the attack value.
    pub fn set_attack(&mut self, sample_rate: SampleRate, attack_seconds: f32) {
        self.attack_multiplier = exponential_multiplier(sample_rate, attack_seconds);
    }

    /// Get the decay value.
    pub fn decay(&self, sample_rate: SampleRate) -> f32 {
        multiplier_to_duration(sample_rate, self.decay_multiplier)
    }

    /// Set the decay.
    pub fn set_decay(&mut self, sample_rate: SampleRate, decay_seconds: f32) {
        self.decay_multiplier = exponential_multiplier(sample_rate, decay_seconds);
    }

    /// Returns the sustain of this [`EnvelopeParams`].
    pub fn sustain(&self) -> f32 {
        self.sustain_amp
    }

    /// Sets the sustain of this [`EnvelopeParams`].
    pub fn set_sustain(&mut self, sustain_amp: f32) {
        debug_assert!(
            (0.0..=1.0).contains(&sustain_amp),
            "0.0 <= {sustain_amp} <= 1.0"
        );
        self.sustain_amp = sustain_amp;
    }

    /// Get the release value in seconds.
    pub fn release(&self, sample_rate: SampleRate) -> f32 {
        multiplier_to_duration(sample_rate, self.release_multiplier)
    }

    /// Sets the release of this [`EnvelopeParams`].
    pub fn set_release(&mut self, sample_rate: SampleRate, release_seconds: f32) {
        self.release_multiplier = exponential_multiplier(sample_rate, release_seconds);
    }
}

/// Handles envelope logic.
#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub struct LinearEnvelope {
    /// The current stage in the envelope.
    stage: Stage,
    /// The current amp.
    amp: f32,
}

/// The stage of the envelope.
#[derive(Copy, Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
enum Stage {
    /// The attack phase.
    #[default]
    Attack,
    /// The decay phase.
    Decay,
    /// The sustain phase.
    Sustain,
    /// The release phase.
    Release,
    /// The final phase which means that the envelope is done and produces no (value of 0.0) signal.
    Done,
}

impl LinearEnvelope {
    /// An envelope that has been completely released and is no longer active.
    pub const INACTIVE: LinearEnvelope = LinearEnvelope {
        stage: Stage::Done,
        amp: 0.0,
    };

    /// Create a new envelope.
    pub fn new() -> LinearEnvelope {
        LinearEnvelope {
            stage: Stage::Attack,
            amp: 0.0,
        }
    }

    /// Get the next sample in the envelope.
    pub fn next_sample(&mut self, params: &LinearEnvelopeParams) -> f32 {
        match self.stage {
            Stage::Attack => {
                self.amp += params.attack_delta;
                if self.amp >= 1.0 {
                    self.amp = 1.0;
                    self.stage = Stage::Decay;
                }
            }
            Stage::Decay => {
                self.amp += params.decay_delta;
                if self.amp <= params.sustain_amp {
                    self.amp = params.sustain_amp;
                    self.stage = Stage::Sustain;
                }
            }
            Stage::Sustain => {}
            Stage::Release => {
                self.amp += params.release_delta;
                if self.amp < 0.0 {
                    self.amp = 0.0;
                    self.stage = Stage::Done;
                }
            }
            Stage::Done => {}
        }
        self.amp
    }

    /// Iterate through many samples.
    pub fn iter_samples<'a>(
        &'a mut self,
        params: &'a LinearEnvelopeParams,
        count: usize,
    ) -> impl 'a + ExactSizeIterator + Iterator<Item = f32> {
        (0..count).map(|_| self.next_sample(params))
    }

    /// Release the envelope and begin the release phase.
    pub fn release(&mut self, params: &LinearEnvelopeParams) {
        self.amp = self.amp.min(params.sustain_amp);
        self.stage = Stage::Release;
    }

    /// Returns true if the envelope is still active.
    pub fn is_active(&self) -> bool {
        self.stage != Stage::Done
    }
}

/// Handles envelope logic.
#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub struct ExponentialEnvelope {
    /// The current stage in the envelope.
    stage: Stage,
    /// The current amp.
    amp: f32,
}

impl ExponentialEnvelope {
    /// An envelope that has been completely released and is no longer active.
    pub const INACTIVE: ExponentialEnvelope = ExponentialEnvelope {
        stage: Stage::Done,
        amp: 0.0,
    };

    /// Create a new envelope.
    pub fn new() -> ExponentialEnvelope {
        ExponentialEnvelope {
            stage: Stage::Attack,
            amp: 1.0,
        }
    }

    /// Get the next sample in the envelope.
    pub fn next_sample(&mut self, params: &ExponentialEnvelopeParams) -> f32 {
        let amp_threshold = 1E-4;
        match self.stage {
            Stage::Attack => {
                self.amp *= params.attack_multiplier;
                if self.amp < amp_threshold {
                    let res = 1.0 - self.amp;
                    self.amp = 1.0;
                    self.stage = Stage::Decay;
                    res
                } else {
                    1.0 - self.amp
                }
            }
            Stage::Decay => {
                self.amp *= params.decay_multiplier;
                if self.amp <= amp_threshold {
                    let res = params.sustain_amp + (1.0 - params.sustain_amp) * self.amp;
                    self.amp = 1.0;
                    self.stage = Stage::Sustain;
                    res
                } else {
                    params.sustain_amp + (1.0 - params.sustain_amp) * self.amp
                }
            }
            Stage::Sustain => params.sustain_amp,
            Stage::Release => {
                self.amp *= params.release_multiplier;
                if self.amp < amp_threshold {
                    self.amp = 0.0;
                    self.stage = Stage::Done;
                    0.0
                } else {
                    self.amp * params.sustain_amp
                }
            }
            Stage::Done => 0.0,
        }
    }

    /// Iterate through many samples.
    pub fn iter_samples<'a>(
        &'a mut self,
        params: &'a ExponentialEnvelopeParams,
        count: usize,
    ) -> impl 'a + ExactSizeIterator + Iterator<Item = f32> {
        (0..count).map(|_| self.next_sample(params))
    }

    /// Release the envelope and begin the release phase.
    pub fn release(&mut self) {
        self.amp = 1.0;
        self.stage = Stage::Release;
    }

    /// Returns true if the envelope is released or done.
    pub fn is_released(&self) -> bool {
        self.stage == Stage::Release || self.stage == Stage::Done
    }

    /// Returns true if the envelope is still active.
    pub fn is_active(&self) -> bool {
        self.stage != Stage::Done
    }
}

const EXPONENTIAL_DECAY_TARGET: f32 = 1.0 / 4.0;

fn exponential_multiplier(sample_rate: SampleRate, duration: f32) -> f32 {
    let m = if duration > 0.0 {
        let frames = duration * sample_rate.sample_rate();
        EXPONENTIAL_DECAY_TARGET.powf(1.0 / frames)
    } else {
        0.0
    };
    debug_assert!((0f32..1f32).contains(&m));
    m
}

fn multiplier_to_duration(sample_rate: SampleRate, multiplier: f32) -> f32 {
    debug_assert!((0f32..1f32).contains(&multiplier));
    if multiplier == 0.0 {
        0.0
    } else {
        let frames = multiplier.log(EXPONENTIAL_DECAY_TARGET).recip();
        frames * sample_rate.seconds_per_sample()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use pretty_assertions::assert_eq;

    #[test]
    fn linear_is_active_until_release() {
        let params = LinearEnvelopeParams::default();
        let base = LinearEnvelope::new();
        {
            let mut active = base;
            for _ in active.iter_samples(&params, 1000) {}
            assert!(active.is_active(), "{:?}", active);
        }
        {
            let mut released = base.clone();
            released.release(&params);
            assert!(released.is_active(), "{:?}", released);
            for _ in released.iter_samples(&params, 1000) {}
            assert!(!released.is_active(), "{:?}", released);
        }
    }

    #[test]
    fn default_envelope() {
        let params = LinearEnvelopeParams::default();
        let sample_rate = SampleRate::new(64.0);
        // The minimum attack is at least 1 frame.
        assert_eq!(params.attack(sample_rate), 1.0 / 64.0);
        assert_eq!(params.decay(sample_rate), 0.0);
        assert_eq!(params.sustain(), 1.0);
        // The minimum release is at least 1 frame.
        assert_eq!(params.release(sample_rate), 1.0 / 64.0);
    }

    #[test]
    fn inactive_envelope_produces_no_signal() {
        let params = LinearEnvelopeParams::default();
        let mut env = LinearEnvelope::INACTIVE;
        let output: Vec<_> = env.iter_samples(&params, 10000).collect();
        assert_eq!(output, vec![0.0; 10000]);
    }

    #[test]
    fn large_attack_takes_long_time_to_reach_1() {
        let sample_rate = SampleRate::new(4.0);
        let params = LinearEnvelopeParams::new(sample_rate, 2.0, 0.0, 1.0, 0.0);
        let mut env = LinearEnvelope::new();
        let output: Vec<_> = env.iter_samples(&params, 10).collect();
        assert_eq!(
            output,
            vec![
                1.0 / 8.0,
                2.0 / 8.0,
                3.0 / 8.0,
                4.0 / 8.0,
                5.0 / 8.0,
                6.0 / 8.0,
                7.0 / 8.0,
                1.0,
                1.0,
                1.0
            ],
            "params={params:?}"
        );
    }

    #[test]
    fn get_and_set_params_produces_consistent_values() {
        let mut params = LinearEnvelopeParams::default();
        let sample_rate = SampleRate::new(64.0);
        params.set_attack(sample_rate, 5.0);
        params.set_decay(sample_rate, 6.0);
        params.set_sustain(sample_rate, 0.7);
        params.set_release(sample_rate, 0.8);
        assert_eq!(params.attack(sample_rate), 5.0);
        assert_eq!(params.decay(sample_rate), 6.0);
        assert_eq!(params.sustain(), 0.7);
        assert_eq!(params.release(sample_rate), 0.8);
    }

    #[test]
    fn zero_second_durations_are_ok() {
        let mut params = LinearEnvelopeParams::default();
        let sample_rate = SampleRate::new(64.0);
        params.set_attack(sample_rate, 0.0);
        params.set_decay(sample_rate, 0.0);
        params.set_sustain(sample_rate, 0.0);
        params.set_release(sample_rate, 0.0);
        // At least 1 frame is required.
        assert_eq!(params.attack(sample_rate), 1.0 / 64.0);
        assert_eq!(params.decay(sample_rate), 0.0);
        assert_eq!(params.sustain(), 0.0);
        assert_eq!(params.release(sample_rate), 0.0);
    }

    #[test]
    #[should_panic]
    fn bad_attack_panics() {
        LinearEnvelopeParams::default().set_attack(SampleRate::new(44100.0), -1.0);
    }

    #[test]
    #[should_panic]
    fn bad_decay_panics() {
        LinearEnvelopeParams::default().set_decay(SampleRate::new(44100.0), -1.0);
    }

    #[test]
    #[should_panic]
    fn bad_sustain_panics() {
        LinearEnvelopeParams::default().set_sustain(SampleRate::new(44100.0), -1.0);
    }

    #[test]
    #[should_panic]
    fn bad_release_panics() {
        LinearEnvelopeParams::default().set_release(SampleRate::new(44100.0), -1.0);
    }

    #[test]
    fn exponential_is_active_until_release() {
        let params = ExponentialEnvelopeParams::default();
        let base = ExponentialEnvelope::new();
        {
            let mut active = base;
            for _ in active.iter_samples(&params, 1000) {}
            assert!(active.is_active(), "{:?}", active);
        }
        {
            let mut released = base.clone();
            released.release();
            assert!(released.is_active(), "{:?}", released);
            for _ in released.iter_samples(&params, 1000) {}
            assert!(!released.is_active(), "{:?}", released);
        }
    }

    #[test]
    fn exponential_default_envelope() {
        let params = ExponentialEnvelopeParams::default();
        let sample_rate = SampleRate::new(64.0);
        assert_eq!(params.attack(sample_rate), 0.0);
        assert_eq!(params.decay(sample_rate), 0.0);
        assert_eq!(params.sustain(), 1.0);
        assert_eq!(params.release(sample_rate), 0.0);
    }

    #[test]
    fn exponential_inactive_envelope_produces_no_signal() {
        let params = ExponentialEnvelopeParams::default();
        let mut env = ExponentialEnvelope::INACTIVE;
        let output: Vec<_> = env.iter_samples(&params, 10000).collect();
        assert_eq!(output, vec![0.0; 10000]);
    }

    #[test]
    fn exponential_large_attack_takes_long_time_to_reach_1() {
        let sample_rate = SampleRate::new(4.0);
        let params = ExponentialEnvelopeParams::new(sample_rate, 2.0, 0.0, 1.0, 0.0);
        let mut env = ExponentialEnvelope::new();
        let output: Vec<_> = env.iter_samples(&params, 10).collect();
        assert!(output[0] < 0.3, "output: {output:?}, params: {params:?}");
        assert!(
            (0.80..0.85).contains(&output[9]),
            "output: {output:?}, params: {params:?}"
        );
        for (a, b) in output.iter().zip(output.iter().skip(1)) {
            assert!(a < b, "{a} < {b}, output: {output:?} params: {params:?}");
        }
    }

    #[test]
    fn exponential_get_and_set_params_produces_consistent_values() {
        let mut params = ExponentialEnvelopeParams::default();
        let sample_rate = SampleRate::new(64.0);
        params.set_attack(sample_rate, 5.0);
        params.set_decay(sample_rate, 6.0);
        params.set_sustain(0.7);
        params.set_release(sample_rate, 8.0);
        assert_eq!(params.attack(sample_rate).round(), 5.0, "attack");
        assert_eq!(params.decay(sample_rate).round(), 6.0, "decay");
        assert_eq!(params.sustain(), 0.7, "sustain");
        assert_eq!(params.release(sample_rate).round(), 8.0, "release");
    }

    #[test]
    fn exponential_zero_second_durations_are_ok() {
        let mut params = ExponentialEnvelopeParams::default();
        let sample_rate = SampleRate::new(64.0);
        params.set_attack(sample_rate, 0.0);
        params.set_decay(sample_rate, 0.0);
        params.set_sustain(0.0);
        params.set_release(sample_rate, 0.0);
        // At least 1 frame is required.
        assert_eq!(params.attack(sample_rate), 0.0);
        assert_eq!(params.decay(sample_rate), 0.0);
        assert_eq!(params.sustain(), 0.0);
        assert_eq!(params.release(sample_rate), 0.0);
    }
}
