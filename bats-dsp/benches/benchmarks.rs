use std::time::Duration;

use bats_dsp::{
    envelope::{
        ExponentialEnvelope, ExponentialEnvelopeParams, LinearEnvelope, LinearEnvelopeParams,
    },
    moog_filter::MoogFilter,
    sample_rate::SampleRate,
    waveform::{Sawtooth, Square, Triangle, Waveform},
};
use criterion::{black_box, criterion_group, criterion_main, Criterion};

const BUFFER_SIZE: usize = 128;
const SAMPLE_RATE: f32 = 44100.0;

fn init_benchmark(c: &mut Criterion) {
    c.benchmark_group("init")
        .warm_up_time(Duration::from_millis(10))
        .measurement_time(Duration::from_millis(100))
        .confidence_level(0.99)
        .bench_function("moog", |b| {
            b.iter(|| MoogFilter::new(SampleRate::new(black_box(SAMPLE_RATE))))
        })
        .bench_function("exponential_envelope", |b| {
            b.iter(|| {
                ExponentialEnvelopeParams::new(
                    black_box(SampleRate::new(SAMPLE_RATE)),
                    black_box(1.0),
                    black_box(1.0),
                    black_box(0.5),
                    black_box(1.0),
                )
            })
        })
        .bench_function("linear_envelope", |b| {
            b.iter(|| {
                LinearEnvelopeParams::new(
                    black_box(SampleRate::new(SAMPLE_RATE)),
                    black_box(1.0),
                    black_box(1.0),
                    black_box(0.5),
                    black_box(1.0),
                )
            })
        });
}

fn process_benchmarks(c: &mut Criterion) {
    c.benchmark_group("process")
        .measurement_time(Duration::from_secs(1))
        .confidence_level(0.99)
        .bench_function("moog_process", |b| {
            let mut f = MoogFilter::new(SampleRate::new(SAMPLE_RATE));
            let mut result = black_box(vec![0f32; BUFFER_SIZE]);
            b.iter(move || {
                for out in result.iter_mut() {
                    *out = f.process(*out);
                }
            });
        })
        .bench_function("process_exponential_envelope", |b| {
            let mut result = black_box(vec![0f32; BUFFER_SIZE]);
            let params = black_box(ExponentialEnvelopeParams::new(
                SampleRate::new(SAMPLE_RATE),
                1.0,
                1.0,
                0.5,
                1.0,
            ));
            b.iter(move || {
                let mut envelope = ExponentialEnvelope::new();
                for out in result.iter_mut() {
                    *out = envelope.next_sample(&params);
                }
            });
        })
        .bench_function("process_linear_envelope", |b| {
            let mut result = black_box(vec![0f32; BUFFER_SIZE]);
            let params = black_box(LinearEnvelopeParams::new(
                SampleRate::new(SAMPLE_RATE),
                1.0,
                1.0,
                0.5,
                1.0,
            ));
            b.iter(move || {
                let mut envelope = LinearEnvelope::new();
                for out in result.iter_mut() {
                    *out = envelope.next_sample(&params);
                }
            });
        })
        .bench_function("process_sawtooth", |b| {
            let mut result = black_box(vec![0f32; BUFFER_SIZE]);
            let mut wave = Sawtooth::new(SampleRate::new(SAMPLE_RATE), 440.0);
            b.iter(move || {
                for out in result.iter_mut() {
                    *out = wave.next_sample();
                }
            });
        })
        .bench_function("process_square", |b| {
            let mut result = black_box(vec![0f32; BUFFER_SIZE]);
            let mut wave = Square::new(SampleRate::new(SAMPLE_RATE), 440.0);
            b.iter(move || {
                for out in result.iter_mut() {
                    *out = wave.next_sample();
                }
            });
        })
        .bench_function("process_triangle", |b| {
            let mut result = black_box(vec![0f32; BUFFER_SIZE]);
            let mut wave = Triangle::new(SampleRate::new(SAMPLE_RATE), 440.0);
            b.iter(move || {
                for out in result.iter_mut() {
                    *out = wave.next_sample();
                }
            });
        });
}

criterion_group!(benches, init_benchmark, process_benchmarks,);
criterion_main!(benches);
